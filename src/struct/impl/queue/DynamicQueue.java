package struct.impl.queue;

import struct.impl.stack.Node;
import main.tp3.ejercicio1.Pen;
import struct.istruct.Queue;

/**
 * Created by Franco Palumbo on 09-Apr-17.
 */
public class DynamicQueue<T> implements Queue<T> {

    private Node backNode;
    private Node frontNode;

    public DynamicQueue() {
        backNode = null;
        frontNode = null;
    }

    public void enqueue(T o) {
        if (isEmpty()) {
            frontNode = backNode = new Node(o);

        } else {
            Node aux = new Node(o);

            backNode.next = aux;
            backNode = aux;
        }
    }

    public T dequeue() {
        if (frontNode == null) {
            System.out.println("Queue is empty");
            return null;
        } else {
            if (frontNode.next == null) {
                Node aux = frontNode;
                frontNode = null;
                return (T) aux.getData();
            } else {
                Node aux = frontNode;
                frontNode = frontNode.next;
                return (T) aux.getData();
            }

        }
    }

    public boolean isEmpty() {
        if (backNode == null && frontNode == null) {
            return true;
        }
        return false;
    }

    @Override
    public int length() {
        return size();
    }

    public int size() {
        Node aux = frontNode;
        int counter = 0;
        if (aux == null) {
            return counter;
        }
        counter++;
        while (aux.hasNext()) {
            counter++;
            aux = aux.next;
        }
        return counter;
    }

    public void empty() {
        frontNode = null;
        backNode = null;
    }

    public static void main(String[] args) {

        Pen pen1 = new Pen("Rojo");
        Pen pen2 = new Pen("Blue");
        Pen pen3 = new Pen("Black");

        DynamicQueue<Pen> cola = new DynamicQueue<>();
        System.out.println("Empty: " + cola.isEmpty());
        cola.enqueue(pen1);
        cola.enqueue(pen2);
        System.out.println("Size: " + cola.size());
        cola.dequeue();
        System.out.println("isEmpty & amountOfInsertions: " + cola.isEmpty() + " & " + cola.size());
        cola.empty();
        System.out.println(cola.size());
        cola.enqueue(pen1);
        cola.enqueue(pen2);
        cola.enqueue(pen3);
        System.out.println(cola.size());
        System.out.println(cola.isEmpty());
        System.out.println(cola.frontNode);
        System.out.println(cola.dequeue());
        System.out.println(cola.size());

    }

}