
package struct.impl.queue;

import struct.istruct.Queue;


/**
 * Created by Matias on 4/5/17.
 */

public class StaticQueue<Q> implements Queue {

    public static void main(String[] args) {

        StaticQueue<String> stringQueue = new StaticQueue<>(5);
        stringQueue.enqueue("Shilton");
        stringQueue.enqueue("Pandu");
        stringQueue.enqueue("Mate");

        System.out.println("el valor que sale de la cola es:\t" + stringQueue.dequeue());
        System.out.println("el valor que sale de la cola es:\t" + stringQueue.dequeue());
        System.out.println("el valor que sale de la cola es:\t" + stringQueue.dequeue());

        stringQueue.enqueue("Mate");
        stringQueue.enqueue("Shilton");
        stringQueue.enqueue("Pandu");
        stringQueue.enqueue("Shilton");
        stringQueue.enqueue("Pandu");

        System.out.println("el valor que sale de la cola es:\t" + stringQueue.dequeue());
        System.out.println("el valor que sale de la cola es:\t" + stringQueue.dequeue());
        System.out.println("el valor que sale de la cola es:\t" + stringQueue.dequeue());

    }

    private int front;
    private int back;
    private int size;
    private int amount;
    private Object[] data;

    public StaticQueue(int size) {
        this.size = size;
        front = 0;
        back = 0;
        amount = 0;
        data = new Object[size];
    }

    @Override
    public void enqueue(Object o) {
        if (amount != size && back <= (size - 1)) {
            data[back] = o;
            back++;
            amount++;
        } else if (back == size && amount < size) {
            back = 0;
            data[back] = o;
            back++;
            amount++;
        } else {        // REVISAR ESTE ELSE
            data[back] = o;
            back++;
            amount++;
        }
    }

    public Object dequeue() {
        if (front == size) {
            front = 0;
        }
        if (isEmpty() && front < size) {
            amount--;
            return (Q) data[front++];
        } else {
            return null;
        }
    }

    public boolean isEmpty() {
        return amount > 0;
    }

    public int length() {
        return size;
    }

    public int size() {
        return amount;
    }

    public void empty() {
        front = back = amount = 0;
    }

}
