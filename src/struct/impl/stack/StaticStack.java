package struct.impl.stack;

import struct.istruct.Stack;

/**
 * Created by Franco Palumbo on 24-Mar-17.
 */
public class StaticStack<T> implements Stack<T> {

    private int capacity;
    private int top;
    private Object[] data;

    /**
     * Creates a Stack(first element is the last to leave, last element is the firs to leave) with a specific capacity
     * and when the Stack is full the Stack will grow, duplicating it´s capacity
     * @param capacity, the amount of elements the Stack will start with
     */
    public StaticStack(int capacity) {
        top = -1;
        this.capacity = capacity;
        data = new Object[capacity];
    }

    /**
     * Adds an element to the beginning of the Stack, moving down the one that was at the top
     * @param o, object that is put at the beginning
     */
    public void push(T o) {
//        if (top + 1 == capacity) {
//            //grow();
//            System.out.println("No hay mas lugar");
//        }
        top++;
        data[top] = o;
    }

    /**
     * Removes the first element of the Stack
     */
    public void pop() {
        top--;
    }

    /**
     * Shows the first element of the Stack
     * @return the element
     */
    public T peek() {
        if (!isEmpty()) {
            return (T)data[top];
        }
        return null;
    }

    /**
     * Shows if the Stack has elements in it
     * @return true if the stack is empty, false otherwise
     */
    public boolean isEmpty() {
        if (top < 0) {
            return true;
        }
        return false;
    }

    /**
     * Shows the amount of elements tha Stack has
     * @return number of elements
     */
    public int size() {
        return top + 1;
    }

    /**
     * Removes all the elements of the Stack
     */
    public void empty() {
        top = -1;
    }

    /**
     * When the Stack is full, duplicates its capacity by creating another Stack withe double of it´s capacity and the
     * passing all the values from one to the other
     */
    private void grow() {
        Object[] data2 = new Object[capacity * 2];
        for (int i = 0; i < capacity; i++) {
            data2[i] = data[i];
        }
        data = data2;
        capacity = capacity * 2;
    }

}
