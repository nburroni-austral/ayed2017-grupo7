package struct.impl.stack;

/**
 * Created by Franco Palumbo on 25-Mar-17.
 */
public class Node<T> {

    private T data;
    public Node next;

    /**
     * Creates a Node that will have an data and a reference to the next Node
     * @param data, Object that will have the Node
     */
    public Node(T data) {
        this.data = data;
    }


    public String toString() {
        return  ""+data;
    }

    /**
     * Shows if the Node has a next Node
     * @return if the next Node is a null return false, otherwise retunr true
     */
    public boolean hasNext() {
        if (next != null) {
            return true;
        }
        return false;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}