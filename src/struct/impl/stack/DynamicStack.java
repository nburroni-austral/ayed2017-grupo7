package struct.impl.stack;

import struct.istruct.Stack;

/**
 * Created by Franco Palumbo on 24-Mar-17.
 */
public class DynamicStack<T> implements Stack<T> {

    private Node<T> topNode;

    /**
     * Creates a DynamicStack that has a topNode that is null
     */
    public DynamicStack() {
        topNode = null;
    }

    /**
     * A new Node 'n' is created which it´s next will be the topNode, the changes the topNode to the new Node 'n'
     * @param o, the data that the new Node will have as a parameter
     */
    public void push(T o) {
        Node<T> aux = new Node<>(o);
        aux.next = topNode;
        topNode = aux;
    }

    public Node getTopNode() {
        return topNode;
    }

    /**
     * Takes the topNode out of the Stack
     */
    public void pop() {
        topNode = topNode.next;
    }

    /**
     * Show the topNode of the Stack
     * @return topNode
     */
    public T peek() {
        return topNode.getData();
    }

    @Override
    public boolean isEmpty() {
        if (topNode == null) {
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        Node aux = topNode;
        int counter = 0;
        if (aux == null) {
            return counter;
        }
        counter++;
        while (aux.hasNext()) {
            counter++;
            aux = aux.next;
        }
        return counter;
    }

    @Override
    public void empty() {
        topNode = null;
    }

}