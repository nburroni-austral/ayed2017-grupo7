package struct.impl.binaryTree;

/**
 * Created by Franco Palumbo on 09-Apr-17.
 */
public class DoubleNode<T> {

    private T element;
    private DoubleNode left;
    private DoubleNode right;

    /**
     * Constructs a double node with its element, left node and right node set to null
     */
    public DoubleNode() {
        element = null;
        left = null;
        right = null;
    }

    public T getElement() {
        return element;
    }

    public DoubleNode<T> getLeft() {
        return left;
    }

    public DoubleNode<T> getRight() {
        return right;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public void setLeft(DoubleNode<T> left) {
        this.left = left;
    }

    public void setRight(DoubleNode<T> right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "DoubleNode{" +
                "element=" + element +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}