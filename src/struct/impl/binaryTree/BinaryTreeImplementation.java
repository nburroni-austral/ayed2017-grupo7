package struct.impl.binaryTree;

import struct.impl.queue.DynamicQueue;
import struct.istruct.BinaryTree;

/**
 * Created by Matias on 4/9/17.
 */
public class BinaryTreeImplementation<T> implements BinaryTree<T> {

    private DoubleNode<T> root;

    /**
     * Constructs an empty binary tree
     */
    public BinaryTreeImplementation() {
        root = null;
    }

    /**
     * Constructs a binary tree with only a root
     *
     * @param t
     */
    public BinaryTreeImplementation(T t) {
        root = new DoubleNode<>();
        root.setElement(t);
        root.setLeft(null);
        root.setRight(null);
    }

    /**
     * Constructs a binary tree with a root, left child and right child
     *
     * @param t
     * @param left
     * @param right
     */
    public BinaryTreeImplementation(T t, BinaryTreeImplementation left, BinaryTreeImplementation right) {
        root = new DoubleNode<>();
        root.setElement(t);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    /**
     * Method that informs if a binary tree is empty
     *
     * @return TRUE if it is empty. Otherwise, FALSE.
     */
    public boolean isEmpty() {
        return (root == null);
    }

    /**
     * Method that returns the element contained in the root of the binary tree
     *
     * @return
     */
    public T getRoot() {
        return root.getElement();
    }

    /**
     * Method that returns the double node root
     *
     * @return
     */
    public DoubleNode<T> getTreeRoot() {
        return root;
    }

    /**
     * Method that returns the left double node of the binary tree as a new binary tree
     *
     * @return
     */
    public BinaryTreeImplementation<T> getLeft() {
        BinaryTreeImplementation tree = new BinaryTreeImplementation();
        tree.root = root.getLeft();
        return tree;
    }

    /**
     * Method that returns the right double node of the binary tree as a new binary tree
     *
     * @return
     */
    public BinaryTreeImplementation<T> getRight() {
        BinaryTreeImplementation tree = new BinaryTreeImplementation();
        tree.root = root.getRight();
        return tree;
    }

    /**
     * Method that prints the binary tree in preorder (root, left, right)
     */
    public void showPreorder() {
        if(root == null) {
            return;
        }
        System.out.print(root.getElement() + " ");
        getLeft().showPreorder();
        getRight().showPreorder();
    }

    /**
     * Method that prints the binary tree in inorder (left, root, right)
     */
    public void showInorder() {
        if(root == null) {
            return;
        }
        getLeft().showInorder();
        System.out.print(root.getElement() + " ");
        getRight().showInorder();
    }

    /**
     * Method that prints the binary tree in postorder (left, right, root)
     */
    public void showPostorder() {
        if(root == null) {
            return;
        }
        getLeft().showPostorder();
        getRight().showPostorder();
        System.out.print(root.getElement() + " ");
    }

    /**
     * Method that prints the binary tree separated by levels
     */
    public void showByLevels() {
        DynamicQueue<BinaryTreeImplementation> q = new DynamicQueue<>();
        q.enqueue(this);
        while (!q.isEmpty()) {
            BinaryTreeImplementation n = q.dequeue();
            System.out.println(n.getRoot());
            if (n.getLeft() != null) {
                q.enqueue(n.getLeft());
            }
            if (n.getRight() != null) {
                q.enqueue(n.getRight());
            }
        }
    }

}