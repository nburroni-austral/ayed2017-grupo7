package struct.impl.binaryTree;

/**
 * Created by Matias on 4/23/17.
 */
public class BinarySearchTreeImplementation<T> {

    private DoubleNode<T> root;

    public BinarySearchTreeImplementation() {
        root = null;
    }

    public boolean isEmpty() {
        return (root == null);
    }

    public T getRoot() {
        return root.getElement();
    }

    public BinarySearchTreeImplementation<T> getLeft() {
        BinarySearchTreeImplementation t = new BinarySearchTreeImplementation<>();
        t.root = root.getLeft();
        return t;
    }

    public BinarySearchTreeImplementation<T> getRight() {
        BinarySearchTreeImplementation t = new BinarySearchTreeImplementation<>();
        t.root = root.getRight();
        return t;
    }

    public boolean exists(T x) {
        return exists(root, x);
    }

    public T getMin() {
        return getMin(root).getElement();
    }

    public T getMax() {
        return getMax(root).getElement();
    }

    public T search(T x) {
        return search(root, x).getElement();
    }

    public void insert(T x) {
        root = insert(root, x);
    }

    public void delete(T x) {
        root = delete(root, x);
    }

    // METODOS PRIVADOS
    private boolean exists(DoubleNode<T> t, T x) {
        if (t == null)
            return false;
        if (((Comparable) x).compareTo(t.getElement()) == 0)
            return true;
        else if (((Comparable) x).compareTo(t.getElement()) < 0)
            return exists(t.getLeft(), x);
        else
            return exists(t.getRight(), x);
    }

    private DoubleNode<T> getMin(DoubleNode<T> t) {
        if (t.getLeft() == null)
            return t;
        else
            return getMin(t.getLeft());
    }

    private DoubleNode<T> getMax(DoubleNode<T> t) {
        if (t.getRight() == null)
            return t;
        else
            return getMax(t.getRight());
    }

    private DoubleNode<T> search(DoubleNode<T> t, T x) {
        if (((Comparable) x).compareTo(t.getElement()) == 0)
            return t;
        else if (((Comparable) x).compareTo(t.getElement()) < 0)
            return search(t.getLeft(), x);
        else
            return search(t.getRight(), x);
    }


    private DoubleNode<T> insert(DoubleNode<T> t, T x) {
        if (t == null) {
            t = new DoubleNode<>();
            t.setElement(x);
        } else if (((Comparable) x).compareTo(t.getElement()) < 0) {
            t.setLeft(insert(t.getLeft(), x));
        } else {
            t.setRight(insert(t.getRight(), x));
        }
        return t;
    }

    private DoubleNode<T> delete(DoubleNode<T> t, T x) {
        if (((Comparable) x).compareTo(t.getElement()) < 0)
            t.setLeft(delete(t.getLeft(), x));
        else if (((Comparable) x).compareTo(t.getElement()) > 0)
            t.setRight(delete(t.getRight(), x));
        else if (t.getLeft() != null && t.getRight() != null) {
            t.setElement(getMin(t.getRight()).getElement());
            t.setRight(deleteMin(t.getRight()));
        } else if (t.getLeft() != null) {
            t = t.getLeft();
        } else {
            t = t.getRight();
        }
        return t;
    }

    private DoubleNode<T> deleteMin(DoubleNode<T> t) {
        if (t.getLeft() != null) {
            t.setLeft(deleteMin(t.getLeft()));
        } else {
            t = t.getRight();
        }
        return t;
    }

}