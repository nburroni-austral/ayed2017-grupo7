package main.tp1.ejercicio2;

import struct.istruct.Comparable;

/**
 * Created by Matias on 3/13/17.
 *
 * Description: Array is imaginary divided into two parts - sorted one and unsorted one.
 * At the beginning, sorted part contains first element of the array and unsorted one contains the rest.
 * At every step, algorithm takes first element in the unsorted part and inserts it to the right place of the sorted one.
 * When unsorted part becomes empty, algorithm stops.
 */
public class InsertionSort {

    public static void main(String[] args) {
        int n = 10000;
        int[] array = new int[n];
        fillArray(array);
        toString(array);
        insertionSort(array);
        toString(array);
    }

    public static void insertionSort(int[] array) {
        for (int i = 1; array.length > i; i++) {
            int currentValue = array[i];
            int j;
            for (j = i; (j > 0) && (array[j - 1] > currentValue); j--) {
                    array[j] = array[j - 1];
            }
            array[j] = currentValue;
        }
    }

    public static <T> void insertionSortGeneric(Comparable<T>[] array) {
        for (int i = 1; array.length > i; i++) {
            Comparable<T> currentValue = array[i];
            int j;
            for (j = i; (j > 0) && (array[j - 1].compareTo(currentValue) == 1); j--) {
                array[j] = array[j - 1];
            }
            array[j] = currentValue;
        }
    }

    public static void fillArray(int[] array) {
        for(int i = 0; i < array.length; i++) {
            array[i] = (int)(i*Math.random());
        }
    }

    public static void toString(int[] array) {
        System.out.println("--------------------");
        for (int i = 0; array.length > i; i++) {
            System.out.println(array[i]);
        }
        System.out.println("--------------------");
    }

}