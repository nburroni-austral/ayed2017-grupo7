package main.tp1.ejercicio2;

import struct.istruct.Comparable;

/**
 * Created by Matias on 3/13/17.
 *
 * Description: Array is imaginary divided into two parts - sorted one and unsorted one.
 * At the beginning, sorted part is empty, while unsorted one contains whole array.
 * At every step, algorithm finds minimal element in the unsorted part and adds it to the end of the sorted one.
 * When unsorted part becomes empty, algorithm stops.
 */
public class SelectionSort {

    public static void main(String[] args) {
        int n = 1000;
        int[] array = new int[n];
        fillArray(array);
        toString(array);
        selectionSort(array);
        toString(array);
    }

    public static void selectionSort(int[] array) {
        for (int i = 0; array.length - 1 > i; i++) {
            int minimum = i;
            for (int j = i + 1; array.length > j; j++) {
                if (array[minimum] > (array[j])) {
                    minimum = j;
                }
            }
            if (minimum != i) {
                int control = array[i];
                array[i] = array[minimum];
                array[minimum] = control;
            }
        }
    }

    public static <T> void selectionSortGeneric(Comparable<T>[] array) {
        for (int i = 0; array.length - 1 > i; i++) {
            int minimum = i;
            for (int j = i + 1; array.length > j; j++) {
                if (array[minimum].compareTo(array[j]) == 1) {
                    minimum = j;
                }
            }
            if (minimum != i) {
                Comparable<T> control = array[i];
                array[i] = array[minimum];
                array[minimum] = control;
            }
        }
    }

    public static void selectionSortRecursive(int[] array, int index) {
        if (index != array.length -1) {
            int minimum = index;
            for (int j = index + 1; array.length > j; j++) {
                if (array[minimum] > (array[j])) {
                    minimum = j;
                }
            }
            if (minimum != index) {
                int control = array[index];
                array[index] = array[minimum];
                array[minimum] = control;
            }
            selectionSortRecursive(array, index + 1);
        }
    }

    public static void fillArray(int[] array) {
        for(int i = 0; i < array.length; i++) {
            array[i] = (int)(i*Math.random());
        }
    }

    public static void toString(int[] array) {
        System.out.println("--------------------");
        for (int i = 0; array.length > i; i++) {
            System.out.println(array[i]);
        }
        System.out.println("--------------------");
    }

}