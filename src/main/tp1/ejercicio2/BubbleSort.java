package main.tp1.ejercicio2;

import struct.istruct.Comparable;


/**
 * Description: Compare each pair of adjacent elements from the beginning of an array and, if they are in reversed order, swap them.
 * If at least one swap has been done, repeat step 1
 */
public class BubbleSort {

    public static void main(String[] args) {
        int n = 10000;
        int[] array = new int[n];
        fillArray(array);
        toString(array);
        bubbleSort(array);
        toString(array);
    }

    public static void bubbleSort(int[] array) {
        boolean swapped = true;
        int j = 0;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < array.length - j; i++) {
                if (array[i] > array[i + 1]) {
                    int control = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = control;
                    swapped = true;
                }
            }
        }
    }

    public static <T> void bubbleSortGeneric(Comparable<T>[] array) {
        boolean swapped = true;
        int j = 0;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < array.length - j; i++) {
                if (array[i].compareTo(array[i + 1]) == 1) {
                    Comparable<T> control = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = control;
                    swapped = true;
                }
            }
        }
    }

    public static void fillArray(int[] array) {
        for(int i = 0; i < array.length; i++) {
            array[i] = (int)(i*Math.random());
        }
    }

    public static void toString(int[] array) {
        System.out.println("--------------------");
        for (int i = 0; array.length > i; i++) {
            System.out.println(array[i]);
        }
        System.out.println("--------------------");
    }

}