package main.tp1.ejercicio1;

/**
 * The Binary Search is a search algorithm that returns if the target value is included in a sorted array or not.
 * Binary search compares the target value to the middle element of the array; if they are unequal,
 * the half in which the target cannot lie is eliminated and the search continues on the remaining half until it is successful or the remaining half is empty.
 * The Binary Search by Index is a Binary Search but returns the value of the position of the target value in a sorted array.
 *
 */
public class BinarySearch {

    public static void main(String[] args){
        int[] array = {2,3,4,5,6,23};
        System.out.println(binarySearchIndex(array,20));
    }

    public static boolean binarySearch(int[] array, int k) {
        boolean returnValue = false;
        int start = 0;
        int finish = array.length - 1;
        while (start != finish) {
            int mid = (start + finish) / 2;
            if (array[mid] == k) {
                returnValue = true;
                break;
            }
            if (array[mid] < k) {
                start = mid;
            }
            if (array[mid] > k) {
                finish = mid;
            }
            if (finish-start==1) {
                if (array[finish] == k) {
                    returnValue=true;
                    break;
                }else{
                    returnValue=false;
                    break;
                }

            }
        }

        return returnValue;
    }

    public static int binarySearchIndex(int[] array, int k) {
        int start = 0;
        int finish = array.length - 1;
        while (start != finish) {
            int mid = (start + finish) / 2;
            if (array[mid] == k) {
                return mid;
            }
            if (array[mid] < k) {
                start = mid;
            }
            if (array[mid] > k) {
                finish = mid;
            }
            if (finish - start == 1) {
                if (array[finish] == k) {
                    return finish;
                }
                if (array[start] == k) {
                    return start;
                } else {
                    return -1;
                }
            }
        }
        return -1;
    }

}