package main.tp1.ejercicio1;

/**
 *
 * The Sequential Search is a search algorithm that searches every element of an array one bye one until the target value is found
 * or the array has ended if the target value is not present in the array.
 * The Sequential Search by Index is a Sequential Search that indicates the position on the array that the value is present, or returns
 * -1 if the value is not present
 *
 */
public class SequentialSearch {
    public static void main(String[] args){
        int[] array = {2,4,5,6,23};
        System.out.println(sequentialSearchIndex(array,4));
    }

    public static boolean sequentialSearch(int[] array, int toFind) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == toFind) {
                return true;
            }
        }
        return false;
    }

    public static int sequentialSearchIndex(int[] array, int toFind) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == toFind) {
                return i;
            }
        }
        return -1;
    }

}
