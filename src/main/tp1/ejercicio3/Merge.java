package main.tp1.ejercicio3;

/**
 * Created by Usuario on 13-Mar-17.
 */
public class Merge {

    public static <T> T[] merge(Comparable<T>[] array1, Comparable<T>[] array2) {

        int sizeOfArray3 = array1.length + array2.length;
        T[] array3 = (T[]) new Object[sizeOfArray3];
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < array1.length && j < array2.length) {
            if (array1[i].compareTo((T) array2[j]) <= 0) {
                array3[k] = (T) array1[i];
                i++;
                k++;
            } else {
                array3[k] = (T) array2[j];
                j++;
                k++;
            }
        }
        if (i == array1.length) {
            while (j != array2.length) {
                array3[k] = (T) array2[j];
                j++;
                k++;
            }
        }
        if (j == array2.length) {
            while (i != array1.length) {
                array3[k] = (T) array1[i];
                i++;
                k++;
            }
        }
        return array3;

    }

}

