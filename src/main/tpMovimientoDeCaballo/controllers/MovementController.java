package main.tpMovimientoDeCaballo.controllers;

import struct.impl.stack.DynamicStack;
import main.tpMovimientoDeCaballo.models.Board;
import main.tpMovimientoDeCaballo.models.Space;
import main.tpMovimientoDeCaballo.views.BoardView;
import main.tpMovimientoDeCaballo.views.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Matias on 3/31/17.
 */
public class MovementController implements ActionListener {

    private Board chessBoard;
    private Window window;
    private BoardView boardView;


    public MovementController() {

        window = new Window(this);
        chessBoard = new Board();
        chessBoard.fillStacks();
        boardView = window.getBoardView();

        window.setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        if (chessBoard.getHorse().getPossibleMovements().size() > 0) {
            emptyLabel(window.getFirstStackPossibleMovementsPanel());
            emptyLabel(window.getSecondStackPossibleMovementsPanel());
            emptyLabel(window.getThirdStackPossibleMovementsPanel());
            emptyLabel(window.getLastStackPossibleMovementsPanel());
            System.out.print(chessBoard.getHorse().getSpace().toString() + "-");

            DynamicStack<Space> auxVisitedSpacesStack = chessBoard.generateStackOfVisitedSpaces();
            boardView.getBoardSquares()[0][0].setIcon(null);
            boardView.getBoardSquares()[chessBoard.getHorse().getCurrentPosition().getY()][chessBoard.getHorse().getCurrentPosition().getX()].setIcon(null);
            chessBoard.moveHorse();
            boardView.getBoardSquares()[chessBoard.getHorse().getCurrentPosition().getY()][chessBoard.getHorse().getCurrentPosition().getX()].setIcon(boardView.getIcon());
            DynamicStack<Space> auxPossibleMovementSpaceStack = chessBoard.generatePossibleMovements();

            System.out.print(chessBoard.getHorse().getSpace().toString() + "\n");

            DynamicStack<Space> auxFirstStackPossibleMovements = chessBoard.getHorse().generateFirstStack();
            DynamicStack<Space> auxSecondStackPossibleMovements = chessBoard.getHorse().generateSecondStack();
            DynamicStack<Space> auxThirdStackPossibleMovements = chessBoard.getHorse().generateThirdStack();
            DynamicStack<Space> auxLastStackPossibleMovements = chessBoard.getHorse().generateLastStack();
            loadPossibleMovementsStacks(auxFirstStackPossibleMovements, auxSecondStackPossibleMovements, auxThirdStackPossibleMovements, auxLastStackPossibleMovements);

        } else {

            System.out.println("Process finished in " + chessBoard.getHorse().getMovementCounter() + " movements");

        }

    }

    /**
     * Method that gathers data from all possible movements stacks and adds the information to its determined panel.
     *
     * @param auxFirstStackPossibleMovements
     * @param auxSecondStackPossibleMovements
     * @param auxThirdStackPossibleMovements
     * @param auxLastStackPossibleMovements
     */
    private void loadPossibleMovementsStacks(DynamicStack<Space> auxFirstStackPossibleMovements, DynamicStack<Space> auxSecondStackPossibleMovements, DynamicStack<Space> auxThirdStackPossibleMovements, DynamicStack<Space> auxLastStackPossibleMovements) {
        DynamicStack<Space> aux = new DynamicStack<>();
        while (auxFirstStackPossibleMovements.size() > 0) {
            window.getFirstStackPossibleMovementsPanel().add(new Label(auxFirstStackPossibleMovements.peek().toString()));
            aux.push(auxFirstStackPossibleMovements.peek());
            auxFirstStackPossibleMovements.pop();
        }
        fillAgainStacks(auxFirstStackPossibleMovements, aux);
        while (auxSecondStackPossibleMovements.size() > 0) {
            window.getSecondStackPossibleMovementsPanel().add(new Label(auxSecondStackPossibleMovements.peek().toString()));
            aux.push(auxSecondStackPossibleMovements.peek());
            auxSecondStackPossibleMovements.pop();
        }
        fillAgainStacks(auxSecondStackPossibleMovements, aux);
        while (auxThirdStackPossibleMovements.size() > 0) {
            window.getThirdStackPossibleMovementsPanel().add(new Label(auxThirdStackPossibleMovements.peek().toString()));
            aux.push(auxThirdStackPossibleMovements.peek());
            auxThirdStackPossibleMovements.pop();
        }
        fillAgainStacks(auxThirdStackPossibleMovements, aux);
        while (auxLastStackPossibleMovements.size() > 0) {
            window.getLastStackPossibleMovementsPanel().add(new Label(auxLastStackPossibleMovements.peek().toString()));
            aux.push(auxLastStackPossibleMovements.peek());
            auxLastStackPossibleMovements.pop();
        }
        fillAgainStacks(auxLastStackPossibleMovements, aux);
    }

    private void fillAgainStacks(DynamicStack<Space> receives, DynamicStack<Space> gives) {
        while (gives.size() != 0) {
            receives.push(gives.peek());
            gives.pop();
        }
    }

    private void emptyLabel(JPanel jPanel) {
        jPanel.removeAll();
    }


    /**
     * Method that changes board square colors to the original black and white
     */
    /*
    private void refreshBoard() {
        for (int i = 0; i < boardView.getBoardSquares().length; i++) {
            for (int j = 0; j < boardView.getBoardSquares()[i].length; j++) {
                if (boardView.getBoardSquares()[i][j].getBackground() == Color.RED || boardView.getBoardSquares()[i][j].getBackground() == Color.ORANGE) {
                    continue;
                }
                if ((j + i) % 2 == 0) {
                    boardView.getBoardSquares()[i][j].setBackground(Color.BLACK);
                    boardView.getBoardSquares()[i][j].setOpaque(true);
                    boardView.getBoardSquares()[i][j].setBorderPainted(false);
                } else {
                    boardView.getBoardSquares()[i][j].setBackground(Color.WHITE);
                    boardView.getBoardSquares()[i][j].setOpaque(true);
                    boardView.getBoardSquares()[i][j].setBorderPainted(false);
                }
            }
        }
    }
    */

    /**
     * Method that changes board square colors.
     * GREEN if the board square is a possible movement space.
     * RED or ORANGE if the board is a visited space.
     *
     * @param auxVisitedSpacesStack
     * @param auxPossibleMovementSpaceStack
     */
    /*
    private void colorBoard(DynamicStack<Space> auxVisitedSpacesStack, DynamicStack<Space> auxPossibleMovementSpaceStack) {
        while (auxVisitedSpacesStack.amountOfInsertions() > 0) {
            if (boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].getBackground() == Color.WHITE) {
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setBackground(Color.ORANGE);
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setText(auxVisitedSpacesStack.peek().toString());
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setMargin(new Insets(0, 0, 0, 0));
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setOpaque(true);
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setBorderPainted(false);
            }
            if (boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].getBackground() == Color.BLACK) {
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setBackground(Color.RED);
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setText(auxVisitedSpacesStack.peek().toString());
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setMargin(new Insets(0, 0, 0, 0));
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setOpaque(true);
                boardView.getBoardSquares()[auxVisitedSpacesStack.peek().getCoordinates().getY()][auxVisitedSpacesStack.peek().getCoordinates().getX()].setBorderPainted(false);
            }
            auxVisitedSpacesStack.pop();
        }

        while (auxPossibleMovementSpaceStack.amountOfInsertions() > 0) {
            boardView.getBoardSquares()[auxPossibleMovementSpaceStack.peek().getCoordinates().getY()][auxPossibleMovementSpaceStack.peek().getCoordinates().getX()].setBackground(Color.GREEN);
            boardView.getBoardSquares()[auxPossibleMovementSpaceStack.peek().getCoordinates().getY()][auxPossibleMovementSpaceStack.peek().getCoordinates().getX()].setOpaque(true);
            boardView.getBoardSquares()[auxPossibleMovementSpaceStack.peek().getCoordinates().getY()][auxPossibleMovementSpaceStack.peek().getCoordinates().getX()].setBorderPainted(false);
            auxPossibleMovementSpaceStack.pop();
        }
    }
    */

    /**
     * Method that refreshes the possible movements panel.
     * All elements from the panel are eliminated and replaced with new labels.
     * If there are no more possible movements, displays an error message.
     */
    /*
    private void fillPossibleMovementsPanel() {
        window.getPossibleMovementsPanel().removeAll();
        window.getPossibleMovementsPanel().revalidate();
        window.getPossibleMovementsPanel().repaint();

        if (chessBoard.getHorse().getPossibleMovements().amountOfInsertions() > 0) {
            for (int i = 0; i < boardView.getBoardSquares().length; i++) {
                for (int j = 0; j < boardView.getBoardSquares()[i].length; j++) {
                    if (boardView.getBoardSquares()[i][j].getBackground() == Color.GREEN) {
                        JLabel possibleMovement = new JLabel(Space.intToChar(j) + "" + (i + 1), SwingConstants.CENTER);
                        possibleMovement.setBackground(Color.GREEN);
                        possibleMovement.setOpaque(true);
                        possibleMovement.setPreferredSize(new Dimension(45, 45));
                        window.getPossibleMovementsPanel().add(possibleMovement);
                    }
                }
            }
        } else {
            window.getPossibleMovementsPanel().removeAll();
            JLabel noMoreMovements = new JLabel("No hay más movimientos disponibles", SwingConstants.CENTER);
            noMoreMovements.setBackground(Color.ORANGE);
            noMoreMovements.setOpaque(true);
            noMoreMovements.setPreferredSize(new Dimension(300, 45));
            window.getPossibleMovementsPanel().add(noMoreMovements);
        }
    }
    */

}