package main.tpMovimientoDeCaballo.views;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Franco Palumbo on 31-Mar-17.
 */
public class BoardView {

    private JButton[][] boardSquares;
    private ImageIcon icon;

    /**
     * Creates a the Bord View of the bord with JButtons and assigning them their initial colour, black, white or a green colour,
     * if the Button is a possible Space where the horse can go in the next movement. Also it sets the icon for the Horse
     */
    public BoardView() {

        boardSquares = new JButton[8][8];

        for (int i = 0; i < boardSquares.length; i++) {
            for (int j = 0; j < boardSquares[i].length; j++) {
                boardSquares[i][j] = new JButton();
            }
        }

        for (int i = 0; i < boardSquares.length; i++) {
            for (int j = 0; j < boardSquares[i].length; j++) {
                if ((j + i) % 2 == 0) {
                    boardSquares[i][j].setBackground(Color.BLACK);
                    boardSquares[i][j].setOpaque(true);
                    boardSquares[i][j].setBorderPainted(false);
                } else {
                    boardSquares[i][j].setBackground(Color.WHITE);
                    boardSquares[i][j].setOpaque(true);
                    boardSquares[i][j].setBorderPainted(false);
                }
            }
        }

        icon = new ImageIcon("src/main/tpMovimientoDeCaballo/resources/Chess_Horse.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);
        boardSquares[0][0].setIcon(icon);

    }

    public JButton[][] getBoardSquares() {
        return boardSquares;
    }

    public ImageIcon getIcon() {
        return icon;
    }

}