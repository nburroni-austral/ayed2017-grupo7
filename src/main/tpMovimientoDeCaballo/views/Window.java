package main.tpMovimientoDeCaballo.views;

import main.tpMovimientoDeCaballo.controllers.MovementController;
import main.tpMovimientoDeCaballo.models.Space;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Franco Palumbo on 30-Mar-17.
 */
public class Window extends JFrame {

    private JPanel firstStackPossibleMovementsPanel;
    private JPanel secondStackPossibleMovementsPanel;
    private JPanel thirdStackPossibleMovementsPanel;
    private JPanel lastStackPossibleMovementsPanel;
    private JPanel possibleMovementsPanel;
    private BoardView boardView;
    private JButton nextStep;

    public Window(MovementController movementController) {

        setTitle("T.P. - Movimiento de Caballo");
        setSize(600, 700);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Constructing mainPanel
        JPanel mainPanel = new JPanel();
        add(mainPanel);
        BoxLayout mainPanelLayout = new BoxLayout(mainPanel, BoxLayout.Y_AXIS);
        mainPanel.setLayout(mainPanelLayout);

        //Constructing Panels
        firstStackPossibleMovementsPanel = new JPanel();
        secondStackPossibleMovementsPanel = new JPanel();
        thirdStackPossibleMovementsPanel = new JPanel();
        lastStackPossibleMovementsPanel = new JPanel();
        possibleMovementsPanel = new JPanel();
        JPanel titlePanel = new JPanel();
        JPanel boardLettersPanel1 = new JPanel();
        JPanel boardLettersPanel2 = new JPanel();
        JPanel boardNumbersPanel1 = new JPanel();
        JPanel boardNumbersPanel2 = new JPanel();
        JPanel boardPanel = new JPanel();
        JPanel boardCompletePanel = new JPanel();
        JPanel nextMovementButtonPanel = new JPanel();

        //Constructing Layouts
        BoxLayout firstStackPossibleMovementsPanelLayout = new BoxLayout(firstStackPossibleMovementsPanel, BoxLayout.Y_AXIS);
        BoxLayout secondStackPossibleMovementsPanelLayout = new BoxLayout(secondStackPossibleMovementsPanel, BoxLayout.Y_AXIS);
        BoxLayout thirdStackPossibleMovementsPanelLayout = new BoxLayout(thirdStackPossibleMovementsPanel, BoxLayout.Y_AXIS);
        BoxLayout lastStackPossibleMovementsPanelLayout = new BoxLayout(lastStackPossibleMovementsPanel, BoxLayout.Y_AXIS);
        BoxLayout boardNumbersPanelLayout1 = new BoxLayout(boardNumbersPanel1, BoxLayout.Y_AXIS);
        BoxLayout boardNumbersPanelLayout2 = new BoxLayout(boardNumbersPanel2, BoxLayout.Y_AXIS);
        FlowLayout boardLettersPanelLayout1 = new FlowLayout();
        FlowLayout boardLettersPanelLayout2 = new FlowLayout();
        FlowLayout possibleMovementsPanelLayout = new FlowLayout();
        GridLayout boardLayout = new GridLayout(8, 8);
        BorderLayout boardCompletePanelLayout = new BorderLayout(0, 0);

        //Setting Layouts
        boardNumbersPanel1.setLayout(boardNumbersPanelLayout1);
        boardNumbersPanel2.setLayout(boardNumbersPanelLayout2);
        boardLettersPanel1.setLayout(boardLettersPanelLayout1);
        boardLettersPanel2.setLayout(boardLettersPanelLayout2);
        firstStackPossibleMovementsPanel.setLayout(firstStackPossibleMovementsPanelLayout);
        secondStackPossibleMovementsPanel.setLayout(secondStackPossibleMovementsPanelLayout);
        thirdStackPossibleMovementsPanel.setLayout(thirdStackPossibleMovementsPanelLayout);
        lastStackPossibleMovementsPanel.setLayout(lastStackPossibleMovementsPanelLayout);
        possibleMovementsPanel.setLayout(possibleMovementsPanelLayout);
        boardPanel.setLayout(boardLayout);
        boardCompletePanel.setLayout(boardCompletePanelLayout);

        //boardLettersPanel content
        //boardLettersPanel1.add(Box.createVerticalStrut(30));
        //boardLettersPanel2.add(Box.createVerticalStrut(30));
        for (int i = 0; i < 8; i++) {
            JLabel letter1 = new JLabel("" + Space.intToChar(i), SwingConstants.CENTER);
            JLabel letter2 = new JLabel("" + Space.intToChar(i), SwingConstants.CENTER);
            boardLettersPanel1.add(Box.createHorizontalStrut(18));
            boardLettersPanel2.add(Box.createHorizontalStrut(18));
            boardLettersPanel1.add(letter1);
            boardLettersPanel2.add(letter2);
            boardLettersPanel1.add(Box.createHorizontalStrut(18));
            boardLettersPanel2.add(Box.createHorizontalStrut(18));
        }

        //boardNumbersPanels content
        for (int i = 0; i < 8; i++) {
            boardNumbersPanel1.add(Box.createVerticalStrut(15));
            boardNumbersPanel2.add(Box.createVerticalStrut(15));
            boardNumbersPanel1.add(Box.createHorizontalStrut(30));
            boardNumbersPanel2.add(Box.createHorizontalStrut(30));
            JLabel number1 = new JLabel("" + (i + 1));
            JLabel number2 = new JLabel("" + (i + 1));
            boardNumbersPanel1.add(number1);
            boardNumbersPanel2.add(number2);
            boardNumbersPanel1.add(Box.createVerticalStrut(15));
            boardNumbersPanel2.add(Box.createVerticalStrut(15));
        }

        //boardCompletePanel content
        boardCompletePanel.add(boardPanel, BorderLayout.CENTER);
        boardCompletePanel.add(boardLettersPanel1, BorderLayout.NORTH);
        boardCompletePanel.add(boardLettersPanel2, BorderLayout.SOUTH);
        boardCompletePanel.add(boardNumbersPanel1, BorderLayout.EAST);
        boardCompletePanel.add(boardNumbersPanel2, BorderLayout.WEST);

        //titlePanels content
        titlePanel.add(new JLabel("Posibles Movimientos", SwingConstants.CENTER));

        //possibleMovementsPanel content
        possibleMovementsPanel.add(firstStackPossibleMovementsPanel);
        possibleMovementsPanel.add(Box.createHorizontalStrut(50));
        possibleMovementsPanel.add(secondStackPossibleMovementsPanel);
        possibleMovementsPanel.add(Box.createHorizontalStrut(50));
        possibleMovementsPanel.add(thirdStackPossibleMovementsPanel);
        possibleMovementsPanel.add(Box.createHorizontalStrut(50));
        possibleMovementsPanel.add(lastStackPossibleMovementsPanel);

        //mainPanel content
        //mainPanel.add(titlePanel2);
        mainPanel.add(Box.createVerticalStrut(15));
        mainPanel.add(boardCompletePanel);
        mainPanel.add(nextMovementButtonPanel);
        mainPanel.add(titlePanel);
        mainPanel.add(possibleMovementsPanel);

        mainPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

        boardView = new BoardView();

        JButton[][] boardSquares = boardView.getBoardSquares();

        //Sets inital values and dimensions for boardPanel
        for (int i = 0; i < boardSquares.length; i++) {
            for (int j = 0; j < boardSquares[i].length; j++) {
                boardSquares[i][j].setMaximumSize(new Dimension(5, 5));
                boardSquares[i][j].setPreferredSize(new Dimension(5, 5));
                boardPanel.add(boardSquares[i][j]);
            }
        }

        boardPanel.setMaximumSize(new Dimension(450, 450));

        boardCompletePanel.setMaximumSize(new Dimension(550, 750));

        //possibleMovement.setBackground(Color.GREEN);
        //possibleMovement.setOpaque(true);
        //possibleMovement.setPreferredSize(new Dimension(45, 45));

        //nextMovementButtonPanel content
        nextStep = new JButton("Next Movement");
        nextStep.setPreferredSize(new Dimension(200, 30));
        nextStep.addActionListener(movementController);
        nextMovementButtonPanel.add(nextStep);

    }

    public BoardView getBoardView() {
        return boardView;
    }

    public JPanel getPossibleMovementsPanel() {
        return possibleMovementsPanel;
    }

    public JPanel getFirstStackPossibleMovementsPanel() {
        return firstStackPossibleMovementsPanel;
    }

    public JPanel getSecondStackPossibleMovementsPanel() {
        return secondStackPossibleMovementsPanel;
    }

    public JPanel getThirdStackPossibleMovementsPanel() {
        return thirdStackPossibleMovementsPanel;
    }

    public JPanel getLastStackPossibleMovementsPanel() {
        return lastStackPossibleMovementsPanel;
    }

    /*public void setFirstStackPossibleMovementsPanel(JPanel firstStackPossibleMovementsPanel) {
        this.firstStackPossibleMovementsPanel = firstStackPossibleMovementsPanel;
    }

    public void setSecondStackpossibleMovementsPanel(JPanel secondStackpossibleMovementsPanel) {
        this.secondStackpossibleMovementsPanel = secondStackpossibleMovementsPanel;
    }

    public void setThirdStackpossibleMovementsPanel(JPanel thirdStackpossibleMovementsPanel) {
        this.thirdStackpossibleMovementsPanel = thirdStackpossibleMovementsPanel;
    }

    public void setLastStackpossibleMovementsPanel(JPanel lastStackpossibleMovementsPanel) {
        this.lastStackpossibleMovementsPanel = lastStackpossibleMovementsPanel;
    }*/
}