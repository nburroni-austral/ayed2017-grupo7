package main.tpMovimientoDeCaballo.models;

import struct.impl.stack.DynamicStack;

/**
 * Created by Franco Palumbo on 30-Mar-17.
 */
public class Board {

    private Horse horse;
    private Space[][] board;
    private DynamicStack<Space> visitedSpaces;
    private Space[] mainVisitedSpace;


    /**
     * Creates a Board of 8 rows and 8 columns and fills every square of the Board with a new Space
     */
    public Board() {
        board = new Space[8][8];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = new Space(new Vector2D(i, j));
            }
        }
        visitedSpaces = new DynamicStack<>();
        horse = new Horse(board[0][0]);
        horse.setPossibleMovements(generatePossibleMovements());
        mainVisitedSpace = new Space[3];
        horse.getSpace().wasVisited();
    }

    /**
     * Generates all the possible movements the Horse can make in his current position.
     *
     * @return a DynamicStack of Spaces full of the Spaces the horse can move into.
     */
    public DynamicStack<Space> generatePossibleMovements() {
        DynamicStack<Space> returnStack = new DynamicStack<>();
        if ((horse.getCurrentPosition().getX() + 1 < 8) && (horse.getCurrentPosition().getY() + 2 < 8)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() + 1, horse.getCurrentPosition().getY() + 2).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() + 1, horse.getCurrentPosition().getY() + 2));
            }
        }
        if ((horse.getCurrentPosition().getX() - 1 >= 0) && (horse.getCurrentPosition().getY() + 2 < 8)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() - 1, horse.getCurrentPosition().getY() + 2).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() - 1, horse.getCurrentPosition().getY() + 2));
            }
        }
        if ((horse.getCurrentPosition().getX() + 2 < 8) && (horse.getCurrentPosition().getY() + 1 < 8)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() + 2, horse.getCurrentPosition().getY() + 1).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() + 2, horse.getCurrentPosition().getY() + 1));
            }
        }
        if ((horse.getCurrentPosition().getX() - 2 >= 0) && (horse.getCurrentPosition().getY() + 1 < 8)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() - 2, horse.getCurrentPosition().getY() + 1).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() - 2, horse.getCurrentPosition().getY() + 1));
            }
        }
        if ((horse.getCurrentPosition().getX() + 2 < 8) && (horse.getCurrentPosition().getY() - 1 >= 0)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() + 2, horse.getCurrentPosition().getY() - 1).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() + 2, horse.getCurrentPosition().getY() - 1));
            }
        }
        if ((horse.getCurrentPosition().getX() - 2 >= 0) && (horse.getCurrentPosition().getY() - 1 >= 0)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() - 2, horse.getCurrentPosition().getY() - 1).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() - 2, horse.getCurrentPosition().getY() - 1));
            }
        }
        if ((horse.getCurrentPosition().getX() + 1 < 8) && (horse.getCurrentPosition().getY() - 2 >= 0)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() + 1, horse.getCurrentPosition().getY() - 2).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() + 1, horse.getCurrentPosition().getY() - 2));
            }
        }
        if ((horse.getCurrentPosition().getX() - 1 >= 0) && (horse.getCurrentPosition().getY() - 2 >= 0)) {
            if (!getSpaceAt(horse.getCurrentPosition().getX() - 1, horse.getCurrentPosition().getY() - 2).isVisited()) {
                returnStack.push(getSpaceAt(horse.getCurrentPosition().getX() - 1, horse.getCurrentPosition().getY() - 2));
            }
        }
        return returnStack;
    }

    /**
     * Returns a space inside the board that matches a given a set of coordinates
     *
     * @param x
     * @param y
     * @return
     * @throws RuntimeException
     */
    private Space getSpaceAt(int x, int y) throws RuntimeException {
        Vector2D v = new Vector2D(x, y);
        for (int i = 0; board.length > i; i++) {
            for (int j = 0; board[i].length > j; j++) {
                if (board[i][j].equalsPosition(v)) {
                    return board[i][j];
                }
            }
        }
        throw new RuntimeException("There are no more spaces available");
    }

    public Horse getHorse() {
        return horse;
    }

    /**
     * Genereates a Stack of Spaces visited by the Horse.
     *
     * @return
     */
    public DynamicStack<Space> generateStackOfVisitedSpaces() {
        DynamicStack<Space> stackOfVisitedSpaces = visitedSpaces;
        return stackOfVisitedSpaces;
    }

    public Space doRandom() {
        DynamicStack<Space> horsePossibleMovements = horse.getPossibleMovements();
        DynamicStack<Space> aux = new DynamicStack<>();
        int number = (int) (Math.random() * (horsePossibleMovements.size() + 1));
        while (number > 0 && horsePossibleMovements.size() > 1) {
            aux.push(horsePossibleMovements.peek());
            horsePossibleMovements.pop();
            number--;
        }
        Space toReturn = horsePossibleMovements.peek();
        horsePossibleMovements.pop();
        while (aux.size() != 0) {
            horsePossibleMovements.push(aux.peek());
            aux.pop();
        }
        horsePossibleMovements.push(toReturn);
        return toReturn;
    }

    public void fillStacks() {
        if (horse.getMovementCounter() == 0) {
            horse.getSpace().wasVisited();
            horse.setFirstStack(horse.getPossibleMovements());
            horse.addMovement();
            horse.setSpace(doRandom());
            horse.setPossibleMovements(generatePossibleMovements());
            mainVisitedSpace[0] = horse.getSpace();
            System.out.println("FirstStackSize " + horse.generateFirstStack().size());
        }
        if (horse.getMovementCounter() == 1) {
            horse.getSpace().wasVisited();
            horse.setSecondStack(horse.getPossibleMovements());
            horse.addMovement();
            horse.setSpace(doRandom());
            horse.setPossibleMovements(generatePossibleMovements());
            mainVisitedSpace[1] = horse.getSpace();
            System.out.println("SecondStackSize " + horse.generateSecondStack().size());
        }
        if (horse.getMovementCounter() == 2) {
            horse.getSpace().wasVisited();
            horse.setThirdStack(horse.getPossibleMovements());
            horse.addMovement();
            horse.setSpace(doRandom());
            horse.setPossibleMovements(generatePossibleMovements());
            mainVisitedSpace[2] = horse.getSpace();
            System.out.println("ThirdStackSize " + horse.generateThirdStack().size());
        }
        if (horse.getMovementCounter() == 3) {
            horse.setLastStack(horse.getPossibleMovements());
            horse.addMovement();
            horse.setSpace(doRandom());
            horse.setPossibleMovements(generatePossibleMovements());
            System.out.println("LastStackSize " + horse.generateLastStack().size());
            horse.resetMovement();
        }
    }

    public void moveHorse() {
        if (horse.getMovementCounter() < 3) {
            horse.setSpace(mainVisitedSpace[horse.getMovementCounter()]);
            horse.addMovement();
            return;
        }
        if (horse.generateFirstStack().size() == 0) {
            System.out.println("PROCESS FINISHED");
            return;
        }
        if (horse.getMovementCounter() > 1 && horse.getSpace().sameSpace(board[0][0])) {
            System.out.println("THE SIZE OF FIRST STACK: " + horse.generateFirstStack().size());
            horse.setSpace(horse.generateFirstStack().peek());
            mainVisitedSpace[0] = horse.getSpace();
            horse.getSpace().wasVisited();
            visitedSpaces.push(horse.getSpace());
            horse.setSecondStack(generatePossibleMovements());
            horse.addMovement();
            return;
        }
        if (horse.generateSecondStack().size() == 0) {
            visitedSpaces.push(horse.getSpace());

            horse.setSpace(board[0][0]);
            horse.getSpace().wasVisited();
            visitedSpaces.push(horse.getSpace());
            horse.generateFirstStack().pop();
            horse.addMovement();
            return;
        }
        if (horse.getSpace().sameSpace(mainVisitedSpace[0])) {
            System.out.println("THE SIZE OF SECOND STACK: " + horse.generateSecondStack().size());
            horse.setSpace(horse.generateSecondStack().peek());
            mainVisitedSpace[1] = horse.getSpace();
            horse.getSpace().wasVisited();
            visitedSpaces.push(horse.getSpace());
            horse.setThirdStack(generatePossibleMovements());
            horse.addMovement();
            return;
        }
        if (horse.generateThirdStack().size() == 0) {
            visitedSpaces.push(horse.getSpace());
            horse.setSpace(mainVisitedSpace[0]);
            horse.getSpace().wasVisited();
            visitedSpaces.push(horse.getSpace());
            horse.generateSecondStack().pop();
            horse.addMovement();
            return;
        }
        if (horse.getSpace().sameSpace(mainVisitedSpace[1])) {
            System.out.println("THE SIZE OF THIRD STACK: " + horse.generateThirdStack().size());
            horse.setSpace(horse.generateThirdStack().peek());
            mainVisitedSpace[2] = horse.getSpace();
            horse.getSpace().wasVisited();
            visitedSpaces.push(horse.getSpace());
            horse.setLastStack(generatePossibleMovements());
            horse.addMovement();
            return;

        }
        if (horse.generateLastStack().size() != 0) {
            if (horse.getSpace().sameSpace(mainVisitedSpace[2])) {
                horse.setSpace(horse.generateLastStack().peek());
                horse.addMovement();
                return;
            } else {
                horse.setSpace(mainVisitedSpace[2]);
                horse.generateLastStack().pop();
                horse.addMovement();
                return;
            }
        }
        if (horse.generateLastStack().size() == 0) {
            visitedSpaces.push(horse.getSpace());
            horse.setSpace(mainVisitedSpace[1]);
            horse.getSpace().wasVisited();
            visitedSpaces.push(horse.getSpace());
            horse.generateThirdStack().pop();
            horse.addMovement();
            return;
        }
    }

}