package main.tpMovimientoDeCaballo.models;

/**
 * Created by Franco Palumbo on 30-Mar-17.
 */
public class Space {

    private Vector2D coordinates;
    private boolean visited;

    /**
     * Creates a new space with a given set of coordinates.
     * Sets visited to false by default
     *
     * @param coordinates given coordinates
     */
    public Space(Vector2D coordinates) {
        this.coordinates = coordinates;
        visited = false;
    }

    /**
     * @return Space coordinates
     */
    public Vector2D getCoordinates() {
        return coordinates;
    }


    /**
     * @return True if space was visited. Otherwise, returns false
     */
    public boolean isVisited() {
        return visited;
    }

    /**
     * Changes space visited value to true
     */
    public void wasVisited() {
        this.visited = true;
    }

    /**
     * Changes space visited value to false
     */
    public void unVisited() {
        this.visited = false;
    }

    /**
     * Method that compares the coordinates of a space with another vector
     *
     * @param v
     * @return True if space coordinates equals vector v. Otherwise, returns false.
     */
    public boolean equalsPosition(Vector2D v) {
        return coordinates.getX() == v.getX() && coordinates.getY() == v.getY();
    }

    /**
     * Method that compares a space with another one
     *
     * @param space
     * @return True if given space equals current space. Otherwise, returns false
     */
    public boolean equals(Space space) {
        return equalsPosition(space.getCoordinates()) && visited == space.isVisited();
    }

    /**
     * @return Space coordinates expressed in a String consisting of a char and an int (Example: A1)
     */
    public String toString() {
        return intToChar(coordinates.getX()) + "" + (coordinates.getY() + 1);
    }

    public static char intToChar(int i) {
        char character;
        switch (i) {
            case 0:
                character = 'A';
                break;
            case 1:
                character = 'B';
                break;
            case 2:
                character = 'C';
                break;
            case 3:
                character = 'D';
                break;
            case 4:
                character = 'E';
                break;
            case 5:
                character = 'F';
                break;
            case 6:
                character = 'G';
                break;
            case 7:
                character = 'H';
                break;
            default:
                character = 'X';
                break;
        }
        return character;
    }

    public boolean sameSpace(Space s) {
        if (this.getCoordinates().getX() == s.getCoordinates().getX() && this.getCoordinates().getY() == s.getCoordinates().getY()) {
            return true;
        } else {
            return false;
        }
    }

}