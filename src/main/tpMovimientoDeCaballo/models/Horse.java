package main.tpMovimientoDeCaballo.models;

import struct.impl.stack.DynamicStack;

/**
 * Created by Franco Palumbo on 30-Mar-17.
 */
public class Horse {

    private Space space;
    private DynamicStack<Space> possibleMovements;
    private DynamicStack<Space> firstStack;
    private DynamicStack<Space> secondStack;
    private DynamicStack<Space> thirdStack;
    private DynamicStack<Space> lastStack;
    private int movementCounter;


    /**
     * Creates a new horse with a given space and an empty stack of possible movements
     *
     * @param space
     */
    public Horse(Space space) {

        this.space = space;
        possibleMovements = new DynamicStack<>();
        firstStack = new DynamicStack<>();
        secondStack = new DynamicStack<>();
        thirdStack = new DynamicStack<>();
        lastStack = new DynamicStack<>();
        movementCounter = 0;

    }

    /**
     * @return horse's current space
     */
    public Space getSpace() {
        return space;
    }

    /**
     * @return horse's current space coordinates
     */
    public Vector2D getCurrentPosition() {
        return space.getCoordinates();
    }

    /**
     * @return horse's possible movements stack
     */
    public DynamicStack<Space> getPossibleMovements() {
        return possibleMovements;
    }

    public int getMovementCounter() {
        return movementCounter;
    }

    /**
     * Changes the horse space for a given one
     *
     * @param space
     */
    public void setSpace(Space space) {
        this.space = space;
    }

    /**
     * Changes the stack of possible movements for a given one
     *
     * @param possibleMovements
     */
    public void setPossibleMovements(DynamicStack<Space> possibleMovements) {
        this.possibleMovements = possibleMovements;
    }

    public void addMovement() {
        movementCounter++;
    }

    public void resetMovement() {
        movementCounter = 0;
    }

    public DynamicStack<Space> generateFirstStack() {
        DynamicStack<Space> returnStack = firstStack;
        return returnStack;
    }

    public DynamicStack<Space> generateSecondStack() {
        DynamicStack<Space> returnStack = secondStack;
        return returnStack;
    }

    public DynamicStack<Space> generateThirdStack() {
        DynamicStack<Space> returnStack = thirdStack;
        return returnStack;
    }

    public DynamicStack<Space> generateLastStack() {
        DynamicStack<Space> returnStack = lastStack;
        return returnStack;
    }

    public void setFirstStack(DynamicStack<Space> firstStack) {
        this.firstStack = firstStack;
    }

    public void setSecondStack(DynamicStack<Space> secondStack) {
        this.secondStack = secondStack;
    }

    public void setThirdStack(DynamicStack<Space> thirdStack) {
        this.thirdStack = thirdStack;
    }

    public void setLastStack(DynamicStack<Space> lastStack) {
        this.lastStack = lastStack;
    }

}