package main.tpMovimientoDeCaballo.models;

/**
 * Created by Matias on 3/31/17.
 */

public class Vector2D {

    private int x;
    private int y;

    public Vector2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}