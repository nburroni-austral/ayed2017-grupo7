package main.tpSudoku.models;

import java.util.Arrays;

/**
 * Created by Franco Palumbo on 01-Apr-17.
 */
public class Sudoku {

    private int[][] originalMatrix;
    private int[][] answerMatrix;

    /**
     * Creates the original Sudoku where the numbers must be set, and the answer of the original Sudoku which must be
     * completed
     */
    public Sudoku() {
        originalMatrix = new int[9][9];
        answerMatrix = new int[9][9];
    }

    /**
     * Completes a Space of the original matrix
     *
     * @param row,    row of the matrix where the number will be set
     * @param column, column of the matrix where the number will be set
     * @param number, int that sets a Space of the matrix
     */
    public void setMatrix(int row, int column, int number) {
        originalMatrix[row][column] = number;
        answerMatrix[row][column] = number;
    }

    public int[][] getOriginalMatrix() {
        return originalMatrix;
    }

    public int[][] getAnswerMatrix() {
        return answerMatrix;
    }

    @Override
    public String toString() {
        return "Sudoku{" +
                "answerMatrix=" + Arrays.toString(answerMatrix) +
                '}';
    }

    /**
     * Shows in console the original matrix
     */
    public void printOriginalSudoku() {
        for (int i = 0; i < originalMatrix.length; i++) {
            for (int j = 0; j < originalMatrix[i].length; j++) {
                System.out.print(originalMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Shows in console the finial matrix
     */
    public void printFinishSudoku() {
        for (int i = 0; i < answerMatrix.length; i++) {
            for (int j = 0; j < answerMatrix[i].length; j++) {
                System.out.print(answerMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * Check if the number that is going to fill in a Space is not repeted in a column.
     *
     * @param s, Space where the number will be filled.
     * @param n, Number that will be filled.
     * @return true if the number that is going to be filled is not repeted in the column, false otherwise.
     */
    public boolean checkColumn(Space s, int n) {
        for (int i = 0; i < 9; i++) {
            if (answerMatrix[i][s.getColumn()] == n) {
                return false;    //The number to put is in the column
            }
        }
        return true;
    }

    /**
     * Check if the number that is going to fill in a Space is not repeted in a row.
     *
     * @param s, Space where the number will be filled.
     * @param n, Number that will be filled.
     * @return true if the number that is going to be filled is not repeted in the row, false otherwise.
     */
    public boolean checkRow(Space s, int n) {
        for (int i = 0; i < 9; i++) {
            if (answerMatrix[s.getRow()][i] == n) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gives the start and end of block columns and rows to the method checkInsideBlock
     *
     * @param s Space where the number will be filled.
     * @param n Number that will be filled.
     * @return true if the number that is going to be filled is not repeted in the block, false otherwise.
     */
    public boolean checkBlock(Space s, int n) {
        if (s.getRow() < 3 && s.getColumn() < 3) {
            return checkInsideBlock(0, 3, 0, 3, n);
        } else if (s.getRow() < 3 && s.getColumn() < 6) {
            return checkInsideBlock(0, 3, 3, 6, n);
        } else if (s.getRow() < 3 && s.getColumn() < 9) {
            return checkInsideBlock(0, 3, 6, 9, n);
        } else if (s.getRow() < 6 && s.getColumn() < 3) {
            return checkInsideBlock(3, 6, 0, 3, n);
        } else if (s.getRow() < 6 && s.getColumn() < 6) {
            return checkInsideBlock(3, 6, 3, 6, n);
        } else if (s.getRow() < 6 && s.getColumn() < 9) {
            return checkInsideBlock(3, 6, 6, 9, n);
        } else if (s.getRow() < 9 && s.getColumn() < 3) {
            return checkInsideBlock(6, 9, 0, 3, n);
        } else if (s.getRow() < 9 && s.getColumn() < 6) {
            return checkInsideBlock(6, 9, 3, 6, n);
        } else if (s.getRow() < 9 && s.getColumn() < 9) {
            return checkInsideBlock(6, 9, 6, 9, n);
        }
        System.out.println("Could not reach this place");
        return true;
    }

    /**
     * Method used for checking if a number is inside a block
     *
     * @param startOfBlockRow,    the row where the block starts
     * @param endOfBlockRow,      the row where the block ends
     * @param startOfBlockColumn, the column where the block starts
     * @param endOfBlockColumn,   the column where the block ends
     * @param n,                  number to be checked if it´s inside the block
     * @return true if the number is not in the block, false otherwise
     */
    public boolean checkInsideBlock(int startOfBlockRow, int endOfBlockRow, int startOfBlockColumn, int endOfBlockColumn, int n) {
        for (int i = startOfBlockRow; i < endOfBlockRow; i++) {
            for (int j = startOfBlockColumn; j < endOfBlockColumn; j++) {
                if (answerMatrix[i][j] == n) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks if the number is valid for the row, column and block of the specific Space
     *
     * @param s     A specific Space
     * @param value the number to be checked
     * @return true if the number can be filled, false otherwise.
     */
    public boolean isValidSpace(Space s, int value) {
        if (checkRow(s, value) && checkColumn(s, value) && checkBlock(s, value)) {
            return true;
        }
        return false;
    }

    /**
     * Gets the next Space
     *
     * @param s actual Space
     * @return next Sapce
     */
    public Space getNextSpace(Space s) {
        int row = s.getRow();
        int col = s.getColumn();
        col++;
        if (col > 8) {
            col = 0;
            row++;
        }
        if (row > 8) {
            return null;
        }

        Space nextSpace = new Space(row, col);
        return nextSpace;
    }

    /**
     * Solves the sudoku
     *
     * @param s actual space
     * @return true if the sudoku was solved, false if the sudoku can´t be solved.
     */
    public boolean solveTheSudoku(Space s) {
        if (s == null) {       //LLego al final
            return true;
        }
        if (answerMatrix[s.getRow()][s.getColumn()] != 0) {
            return solveTheSudoku(getNextSpace(s));
        }
        for (int i = 1; i <= 9; i++) {
            boolean valid = isValidSpace(s, i);
            if (valid == false) {
                continue;
            }
            answerMatrix[s.getRow()][s.getColumn()] = i;

            boolean solved = solveTheSudoku(getNextSpace(s));

            if (solved) {
                return true;
            } else {
                answerMatrix[s.getRow()][s.getColumn()] = 0;
            }
        }
        return false;
    }

    /**
     * Shows the original matrix and fills the answer matrix and also shows it in console
     */
    public void showSudoku() {
        Space s = new Space(0, 0);
        if (!checkIfSudokuIsSetCorrectly(s)) {
            System.out.println("The Sudoku is incorrectly set");
            return;
        }
        if (solveTheSudoku(s)) {
            System.out.println("Original: ");
            this.printOriginalSudoku();
            System.out.println("//------------//");
            System.out.println("Solution: ");
            this.printFinishSudoku();
        } else {
            System.out.println("Can´t be solved");
        }
    }

    /**
     * Recursive method that returns whether an added int to the original matrix is valid.
     * Method ends once the end of the original matrix is reached (space s = null)
     *
     * @param s space inside the matrix being analyzed
     * @return TRUE if the value is valid. Otherwise returns FALSE
     */
    public boolean checkIfSudokuIsSetCorrectly(Space s) {
        if (s == null) {
            return true;
        } else {
            if (originalMatrix[s.getRow()][s.getColumn()] != 0) {
                int num = originalMatrix[s.getRow()][s.getColumn()];
                originalMatrix[s.getRow()][s.getColumn()] = 0;
                answerMatrix[s.getRow()][s.getColumn()] = 0;
                boolean valid = isValidSpace(s, num);
                if (valid == false) {
                    originalMatrix[s.getRow()][s.getColumn()] = num;
                    answerMatrix[s.getRow()][s.getColumn()] = num;
                    return false;

                } else {
                    originalMatrix[s.getRow()][s.getColumn()] = num;
                    answerMatrix[s.getRow()][s.getColumn()] = num;
                    return checkIfSudokuIsSetCorrectly(getNextSpace(s));
                }
            } else {
                return checkIfSudokuIsSetCorrectly(getNextSpace(s));
            }
        }
    }

}