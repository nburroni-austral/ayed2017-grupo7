package main.tpSudoku.models;

/**
 * Created by Franco Palumbo on 02-Apr-17.
 */
public class Space {

    private int row;
    private int column;

    public Space(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return "Space{" +
                "row=" + row +
                ", column=" + column +
                '}';
    }

}
