package main.tpSudoku.controllers;

import main.tpSudoku.models.Sudoku;
import main.tpSudoku.views.Window;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Matias on 4/3/17.
 */
public class SudokuController {

    private Window window;
    private Sudoku sudoku;

    public SudokuController() {

        window = new Window(this);
        sudoku = new Sudoku();
        window.setVisible(true);

        window.addGetSolutionListener(new getSolutionListener());
        for (int i = 0; i < window.getSudokuSquares().length; i++) {
            for (int j = 0; j < window.getSudokuSquares()[i].length; j++) {
                window.addOrigialMatrixListener(new userInputListener(), i, j);
            }
        }

    }

    class userInputListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            Object button = e.getSource();
            for (int i = 0; i < window.getSudokuSquares().length; i++) {
                for (int j = 0; j < window.getSudokuSquares()[i].length; j++) {
                    if (button.equals(window.getSudokuSquares()[i][j]) && window.getSudokuSquares()[i][j].getText().equals("") && !window.getUserInput().getText().equals("") && sudoku.getOriginalMatrix()[i][j] == 0) {
                        int newNum = window.getUserInput().getText().charAt(0) - 48;
                        sudoku.setMatrix(i, j, newNum);
                        window.getSudokuSquares()[i][j].setText("" + newNum);
                        break;
                    } else if (button.equals(window.getSudokuSquares()[i][j])) {
                        window.getSudokuSquares()[i][j].setText("");
                        sudoku.setMatrix(i, j, 0);
                        break;
                    }
                }
            }

        }

    }

    class getSolutionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            sudoku.showSudoku();
            boolean sudokuIsFinished = true;
            for (int i = 0; i < window.getSudokuSquares().length; i++) {
                for (int j = 0; j < window.getSudokuSquares()[i].length; j++) {
                    if (!window.getSudokuSquares()[i][j].getText().equals("" + sudoku.getAnswerMatrix()[i][j])) {
                        sudokuIsFinished = false;
                    }
                    if (sudoku.getAnswerMatrix()[i][j] != 0) {
                        window.getSudokuSquares()[i][j].setText("" + sudoku.getAnswerMatrix()[i][j]);
                        sudoku.setMatrix(i, j, sudoku.getAnswerMatrix()[i][j]);
                    }
                }
            }
            if (sudokuIsFinished) {
                System.out.println("Sudoku completed correctly, congratulations");
            }

        }

    }

}