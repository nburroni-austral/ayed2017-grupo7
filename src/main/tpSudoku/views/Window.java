package main.tpSudoku.views;

import main.tpSudoku.controllers.SudokuController;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * Created by Franco Palumbo on 01-Apr-17.
 */
public class Window extends JFrame {

    private JFormattedTextField userInput;
    private JButton[][] sudokuSquares;
    private JButton getSolution;

    public Window(SudokuController sudokuController) {

        setTitle("T.P. - Sudoku");
        setSize(450, 600);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        sudokuSquares = new JButton[9][9];
        getSolution = new JButton("Resolver Sudoku");

        //Constructing mainPanel
        JPanel mainPanel = new JPanel();
        BoxLayout mainPanelLayout = new BoxLayout(mainPanel, BoxLayout.Y_AXIS);
        mainPanel.setLayout(mainPanelLayout);
        add(mainPanel);

        //Constructing Panels
        JPanel userInputPanel = new JPanel();
        JPanel sudokuSquaresPanel = new JPanel();
        JPanel getSolutionPanel = new JPanel();

        //Constructing Layouts
        FlowLayout userInputPanelLayout = new FlowLayout();
        GridLayout sudokuSquaresLayout = new GridLayout(9, 9, 5, 5);

        //Setting Layouts
        userInputPanel.setLayout(userInputPanelLayout);
        sudokuSquaresPanel.setLayout(sudokuSquaresLayout);


        //sudokuSquaresPanel content
        for (int i = 0; i < sudokuSquares.length; i++) {
            for (int j = 0; j < sudokuSquares[i].length; j++) {
                sudokuSquares[i][j] = new JButton();
                sudokuSquares[i][j].setPreferredSize(new Dimension(37, 37));
                sudokuSquares[i][j].setMargin(new Insets(0, 0, 0, 0));
            }
        }

        for (int i = 0; i < sudokuSquares.length; i++) {
            for (int j = 0; j < sudokuSquares[i].length; j++) {
                sudokuSquaresPanel.add(sudokuSquares[i][j]);
            }
        }

        sudokuSquaresPanel.setMaximumSize(new Dimension(375,375));

        //userInputPanel content
        JLabel title = new JLabel("Ingrese un Número del 1 al 9:\t");
        title.setHorizontalAlignment(SwingConstants.CENTER);

        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(1);
        formatter.setMaximum(9);
        formatter.setAllowsInvalid(false);
        userInput = new JFormattedTextField(formatter);
        userInput.setHorizontalAlignment(JFormattedTextField.CENTER);
        userInput.setPreferredSize(new Dimension(30, 30));
        userInput.setEditable(true);

        JLabel message = new JLabel("Seleccione los Cuadrados en los cuales Desea Ingresar el Número");

        userInputPanel.add(title);
        userInputPanel.add(userInput);
        userInputPanel.add(message);
        userInputPanel.add(Box.createVerticalStrut(45));

        //getSolutionPanel content
        getSolution.setPreferredSize(new Dimension(375, 45));
        getSolutionPanel.add(getSolution);

        //mainPanel content
        mainPanel.add(userInputPanel);
        mainPanel.add(sudokuSquaresPanel);
        mainPanel.add(getSolutionPanel);

        mainPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

    }

    public JFormattedTextField getUserInput() {
        return userInput;
    }

    public JButton[][] getSudokuSquares() {
        return sudokuSquares;
    }

    public void addGetSolutionListener(ActionListener cal) {
        getSolution.addActionListener(cal);
    }

    public void addOrigialMatrixListener(ActionListener cal, int i, int j) {
        sudokuSquares[i][j].addActionListener(cal);
    }

}