package main.tp10.ejercicio2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Matias on 5/28/17.
 */
public class CharacterCounter {

    public static void main(String[] args) {
        run();
    }

    public static void run() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese el nombre del archivo que desea analizar:");
        String fileName = scanner.nextLine();

        System.out.println("Ingrese el caracter que desea buscar en el archivo:");
        char character = scanner.next().charAt(0);

        System.out.println("El caracter " + character + " aparece " + charOccurrences(character, fileName) + " veces en el archivo " + fileName + ".");

    }

    public static int charOccurrences(char character, String fileName) {
        int counter = 0;
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == character) {
                        counter++;
                    }
                }
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return counter;
    }

}
