package main.tp10.ejercicio3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Matias on 5/28/17.
 */
public class CaseConverter {

    public static void main(String[] args) {
        run();
    }

    public static void run() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Escriba el nombre del archivo que desea convertir: ");
        String fileToConvert = scanner.next();
        String fileConverted = fileToConvert + "Converted";

        System.out.println("Desea convertir el texto a mayuscula? Ingrese 'mayuscula'");
        System.out.println("Desea convertir el texto a minuscula? Ingrese 'minuscula'");
        System.out.println("Si desea cerrar el programa, Ingrese 0");

        String command = scanner.next();

        while (!command.equals("0")) {
            if (command.equals("mayuscula")) {
                convertFile(fileToConvert, fileConverted, true);
                System.out.println("Desea convertir el texto a minuscula? Ingrese 'minuscula'");
                System.out.println("Si desea cerrar el programa, Ingrese 0");
                command = scanner.next();
            } else if (command.equals("minuscula")) {
                convertFile(fileToConvert, fileConverted, false);
                System.out.println("Desea convertir el texto a mayuscula? Ingrese 'mayuscula'");
                System.out.println("Si desea cerrar el programa, Ingrese 0");
                command = scanner.next();
            } else {
                System.out.println("El comando no es válido");
                System.out.println("Desea convertir el texto a mayuscula? Ingrese 'mayuscula'");
                System.out.println("Desea convertir el texto a minuscula? Ingrese 'minuscula'");
                System.out.println("Si desea cerrar el programa, Ingrese 0");
                command = scanner.next();
            }
        }

    }

    private static void convertFile(String fileToConvert, String fileConverted, boolean toUpperCase) {
        try {
            FileReader fr = new FileReader(fileToConvert);
            FileWriter fw = new FileWriter(fileConverted);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            if (toUpperCase) {
                while (line != null) {
                    fw.write(line.toUpperCase() + "\n");
                    line = br.readLine();
                }
                fw.close();
                br.close();
            } else {
                while (line != null) {
                    fw.write(line.toLowerCase() + "\n");
                    line = br.readLine();
                }
                fw.close();
                br.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
