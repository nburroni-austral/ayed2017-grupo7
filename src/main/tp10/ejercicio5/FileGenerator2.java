package main.tp10.ejercicio5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Matias on 5/28/17.
 */
public class FileGenerator2 {

    public static void main(String[] args) {
        run();
    }

    public static void run() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Escriba el nombre del archivo que desea ordenar: ");
        String fileName = scanner.next();

        System.out.println("Escriba el numero de la poblacion: ");
        Double population = scanner.nextDouble();

        System.out.println("Desea escrbir PBI o POB?");
        String command = scanner.next();

        modifiedCountryFileSorter(fileName, command, population);

    }

    public static void modifiedCountryFileSorter(String fileName, String command, double populationCriteria) {
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fr1 = new FileWriter(fileName + " (Below " + populationCriteria + " Million)");
            FileWriter fr2 = new FileWriter(fileName + " (Above " + populationCriteria + " Million)");
            String[] fields;
            String country;
            Double population;
            String pbi;
            String line = br.readLine();
            while (line != null) {
                fields = line.split(" ");
                country = fields[0];
                population = Double.parseDouble(fields[1]);
                pbi = fields[2];
                if (command.toUpperCase().equals("PBI")) {
                    if (population < populationCriteria) {
                        fr1.write(country + " " + pbi + "\n");
                    } else {
                        fr2.write(country + " " + pbi + "\n");
                    }
                } else if (command.toUpperCase().equals("POB")) {
                    if (population < populationCriteria) {
                        fr1.write(country + " " + population + "\n");
                    } else {
                        fr2.write(country + " " + population + "\n");
                    }
                } else {
                    if (population < populationCriteria) {
                        fr1.write(line + "\n");
                    } else {
                        fr2.write(line + "\n");
                    }
                }
                line = br.readLine();
            }
            fr1.close();
            fr2.close();
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}