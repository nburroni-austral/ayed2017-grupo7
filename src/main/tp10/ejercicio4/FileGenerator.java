package main.tp10.ejercicio4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Matias on 5/28/17.
 */
public class FileGenerator {

    public static void main(String[] args) {
        run();
    }

    public static void run() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Escriba el nombre del archivo que desea ordenar: ");
        String fileName = scanner.next();

        fileSorter(fileName);

    }

    public static void fileSorter(String fileName) {
        try {
            FileWriter fr1 = new FileWriter(fileName + " (Below 30 Million)");
            FileWriter fr2 = new FileWriter(fileName + " (Above 30 Million)");
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            String[] fields;
            Double population;
            while (line != null) {
                fields = line.split(" ");
                population = Double.parseDouble(fields[1]);
                if (population < 30) {
                    fr1.write(line + "\n");
                } else {
                    fr2.write(line + "\n");
                }
                line = br.readLine();
            }
            fr1.close();
            fr2.close();
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
