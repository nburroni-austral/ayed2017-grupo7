package main.tp10.ejercicio1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Matias on 5/28/17.
 */
public class Counter {

    public static void main(String[] args) {
        run();
    }

    public static void run() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese C para obtener la cantidad de caracteres de un archivo");
        System.out.println("Ingrese L para obtener la cantidad de lineas de un archivo");
        System.out.println("Ingrese 0 para cerrar el programa");

        String command = scanner.nextLine();

        while (!command.equals("0")) {
            System.out.println("Ingrese el nombre del archivo");
            String fileName = scanner.next();
            if (command.equals("C")) {
                System.out.println("El numero de caracteres es: " + charCounter(fileName));
                System.out.println("Ingrese C para obtener la cantidad de caracteres de un archivo");
                System.out.println("Ingrese L para obtener la cantidad de lineas de un archivo");
                System.out.println("Ingrese 0 para cerrar el programa");
                command = scanner.nextLine();
            } else if (command.equals("L")) {
                System.out.println("El numero de lineas es: " + lineCounter(fileName));
                System.out.println("Ingrese C para obtener la cantidad de caracteres de un archivo");
                System.out.println("Ingrese L para obtener la cantidad de lineas de un archivo");
                System.out.println("Ingrese 0 para cerrar el programa");
                command = scanner.nextLine();
            } else {
                System.out.println("Ingrese C para obtener la cantidad de caracteres de un archivo");
                System.out.println("Ingrese L para obtener la cantidad de lineas de un archivo");
                System.out.println("Ingrese 0 para cerrar el programa");
                command = scanner.nextLine();
            }
        }
    }

    private static int charCounter(String fileName) {
        int chars = 0;
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) != ' ') {
                        chars++;
                    }
                }
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return chars;
    }

    private static int lineCounter(String fileName) {
        int lines = 0;
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                lines++;
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return lines;
    }

}
