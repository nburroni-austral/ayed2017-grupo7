package main.tp11;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Matias on 28-Jun-17
 */

public class Main {

    public static void main(String[] args) {
        try {
            BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
            bookLibrary.write(new Book("Angels and Demons", "Dan Brown", "Planeta", 2002, true));
            bookLibrary.write(new Book("The Da Vinci Code", "Dan Brown", "Planeta", 2006, true));
            bookLibrary.write(new Book("The Lord of the Rings", "J.R.R. Tolkien", "Minotauro", 1966, false));
            bookLibrary.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        Scanner myScanner = new Scanner(System.in);
        System.out.println();
        System.out.println("Welcome to your book library \n");
        System.out.println("Enter 1 to add a book");
        System.out.println("Enter 2 to delete a book");
        System.out.println("Enter 3 to modify a book");
        System.out.println("Enter 4 to show specific information");
        System.out.println("Enter 5 to show all books");
        System.out.println("Enter 0 to exit");
        System.out.print("\nWhat would you like to do? ");
        int selectedNumber = myScanner.nextInt();
        while (selectedNumber != 0) {
            if (selectedNumber == 1) {
                System.out.println();
                System.out.println("Enter the information of the book you would like to add");
                System.out.print("\nBook's title: ");
                String title = myScanner.next();
                title += myScanner.nextLine();
                System.out.print("'" + title + "' author's name: ");
                String author = myScanner.next();
                author += myScanner.nextLine();
                System.out.print("'" + title + "' publisher's name: ");
                String publisher = myScanner.next();
                publisher += myScanner.nextLine();
                System.out.print("'" + title + "' year released: ");
                int year = myScanner.nextInt();
                Book book = new Book(title, author, publisher, year, true);
                try {
                    addBook(book);
                } catch (IOException e) {
                    e.getMessage();
                }
            }
            if (selectedNumber == 2) {
                System.out.println();
                System.out.println("Enter the information of the book you would like to delete");
                System.out.print("\nBook's title: ");
                String title = myScanner.next();
                title += myScanner.nextLine();
                System.out.print("'" + title + "' author's name: ");
                String author = myScanner.next();
                author += myScanner.nextLine();
                try {
                    deleteBook(title, author);
                } catch (IOException e) {
                    e.getMessage();
                }
            }
            if (selectedNumber == 3) {
                System.out.println();
                System.out.println("Enter the information of the book you would like to modify");
                System.out.print("\nBook's title: ");
                String title = myScanner.next();
                title += myScanner.nextLine();
                System.out.print("'" + title + "' author's name: ");
                String author = myScanner.next();
                author += myScanner.nextLine();
                System.out.println();
                System.out.println("Enter the new information of the book");
                System.out.print("\nBook's title: ");
                String newTitle = myScanner.next();
                newTitle += myScanner.nextLine();
                System.out.print("'" + title + "' author's name: ");
                String newAuthor = myScanner.next();
                newAuthor += myScanner.nextLine();
                System.out.print("'" + title + "' publisher's name: ");
                String newPublisher = myScanner.next();
                newPublisher += myScanner.nextLine();
                System.out.print("'" + title + "' year released: ");
                int newYear = myScanner.nextInt();
                try {
                    modifyBook(title, author, new Book(newTitle, newAuthor, newPublisher, newYear, true));
                } catch (IOException e) {
                    e.getMessage();
                }
            }
            if (selectedNumber == 4) {
                System.out.println();
                System.out.println("Enter 1 to show a specific book");
                System.out.println("Enter 2 to show amount of books in your library");
                System.out.println("Enter 3 to show amount of books by a specific author in your library");
                System.out.println("Enter 4 to show amount of books by a specific publisher in your library");
                System.out.println("Enter 5 to go back");
                System.out.print("\nWhat would you like to do? ");
                int selectedNumber2 = myScanner.nextInt();
                while(selectedNumber2 != 5) {
                    if (selectedNumber2 == 1) {
                        System.out.println();
                        System.out.println("Enter the information of the book you would like to search for");
                        System.out.print("\nBook's title: ");
                        String title = myScanner.next();
                        title += myScanner.nextLine();
                        System.out.print("'" + title + "' author's name: ");
                        String author = myScanner.next();
                        author += myScanner.nextLine();
                        try {
                            Book book = searchBook(title, author);
                            if(book != null) {
                                System.out.println(book);
                            } else {
                                System.out.println("Sorry, we couldn't find that book");
                            }
                        } catch (IOException e) {
                            e.getMessage();
                        }
                    }
                    if (selectedNumber2 == 2) {
                        System.out.println();
                        try {
                            System.out.println("There are " + amountOfBooks() + " books in your library");
                        } catch (IOException e) {
                            e.getMessage();
                        }
                    }
                    if (selectedNumber2 == 3) {
                        System.out.println();
                        System.out.print("\nEnter the name of the author: ");
                        String author = myScanner.next();
                        author += myScanner.nextLine();
                        try {
                            System.out.println("There are " + booksByAuthor(author) + " books in your library written by " + author);
                        } catch (IOException e) {
                            e.getMessage();
                        }
                    }
                    if (selectedNumber2 == 4) {
                        System.out.println();
                        System.out.print("\nEnter the name of the publisher: ");
                        String publisher = myScanner.next();
                        publisher += myScanner.nextLine();
                        try {
                            System.out.println("There are " + booksByPublisher(publisher) + " books in your library published by " + publisher);
                        } catch (IOException e) {
                            e.getMessage();
                        }
                    }
                    System.out.println();
                    System.out.println("Enter 1 to show a specific book");
                    System.out.println("Enter 2 to show amount of books in your library");
                    System.out.println("Enter 3 to show amount of books by a specific author in your library");
                    System.out.println("Enter 4 to show amount of books by a specific publisher in your library");
                    System.out.println("Enter 5 to go back");
                    System.out.print("\nWhat would you like to do? ");
                    selectedNumber2 = myScanner.nextInt();
                }
            }
            if (selectedNumber == 5) {
                try {
                    showAllBooks();
                } catch (IOException e) {
                    e.getMessage();
                }
            }
            System.out.println();
            System.out.println("Welcome to your book library \n");
            System.out.println("Enter 1 to add a book");
            System.out.println("Enter 2 to delete a book");
            System.out.println("Enter 3 to modify a book");
            System.out.println("Enter 4 to show specific information");
            System.out.println("Enter 5 to show all books");
            System.out.println("Enter 0 to exit");
            System.out.print("\nWhat would you like to do? ");
            selectedNumber = myScanner.nextInt();
        }
    }

    private static void addBook(Book book) throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        bookLibrary.endOfFile();
        bookLibrary.write(book);
        bookLibrary.close();
    }

    private static void deleteBook(String title, String author) throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        bookLibrary.deleteBook(title, author);
    }

    private static void modifyBook(String title, String author, Book newBook) throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        bookLibrary.modifyBook(title, author, newBook);
    }

    private static Book searchBook(String title, String author) throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        return bookLibrary.listSpecificBook(title, author);
    }

    private static int booksByAuthor(String author) throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        return bookLibrary.amountOfBooksByAuthor(author);
    }

    private static int booksByPublisher(String publisher) throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        return bookLibrary.amountOfBooksByPublisher(publisher);
    }

    private static long amountOfBooks() throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        long l = 0;
        return l;
    }

    private static void showAllBooks() throws IOException {
        BookLibrary bookLibrary = new BookLibrary("src/main/tp11/MyLibrary.txt");
        bookLibrary.listBooks();
    }

}
