package main.tp11;

import java.io.*;

import static java.lang.Integer.parseInt;

/**
 * Created by Matias on 28-Jun-17
 */

public class BookLibrary {

    private RandomAccessFile raf;

    public BookLibrary(String fileName) throws FileNotFoundException {
        raf = new RandomAccessFile(new File(fileName), "rw");
    }

    public Book read(String currentLine) throws IOException {
        String newTitle = currentLine;
        String newAuthor = raf.readLine();
        String newPublisher = raf.readLine();
        int newYear = raf.readInt();
        raf.readLine();
        boolean isActive = raf.readBoolean();
        raf.readLine();

        return new Book(newTitle, newAuthor, newPublisher, newYear, isActive);
    }

    public void write(Book book) throws IOException {
        raf.writeBytes(book.getTitle() + "\n");
        raf.writeBytes(book.getAuthor() + "\n");
        raf.writeBytes(book.getPublisher() + "\n");
        raf.writeInt(book.getYear());
        raf.writeBytes("\n");
        raf.writeBoolean(book.isActive());
        raf.writeBytes("\n");
    }

    public void close() throws IOException {
        raf.close();
    }

    public long length() throws IOException {
        return raf.length();
    }

    public void startOfFile() throws IOException {
        raf.seek(0);
    }

    public void endOfFile() throws IOException {
        raf.seek(raf.length());
    }

    public void deleteBook(String title, String author) throws IOException {
        Book book = listSpecificBook(title, author);
        long index = listSpecificBookIndex(title, author);
        if (book != null) {
            book.toggleIsActive();
            raf.seek(index);
            write(book);
            System.out.println("Your book was deleted");
        }
    }

    public void modifyBook(String title, String author, Book newBook) throws IOException {
        Book book = listSpecificBook(title, author);
        long index = listSpecificBookIndex(title, author);
        if (book != null) {
            book.setTitle(newBook.getTitle());
            book.setAuthor(newBook.getAuthor());
            book.setPublisher(newBook.getPublisher());
            book.setYear(newBook.getYear());
            raf.seek(index);
            write(book);
            System.out.println("Your book was modified");
        }
    }

    public Book listSpecificBook(String title, String author) throws IOException {
        Book book;
        try {
            startOfFile();
            String currentLine = raf.readLine();
            while (currentLine != null) {
                book = read(currentLine);
                if (book.isActive() && book.getTitle().equals(title) && book.getAuthor().equals(author)) {
                    System.out.println("Your book was found!");
                    System.out.println(book.toString());
                    return book;
                }
                currentLine = raf.readLine();
            }
            close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Your book was not found!");
        }
        return null;
    }

    public long listSpecificBookIndex(String title, String author) throws IOException {
        Book book;
        try {
            startOfFile();
            String currentLine = raf.readLine();
            long index = raf.getFilePointer();
            while (currentLine != null) {
                book = read(currentLine);
                if (book.isActive() && book.getTitle().equals(title) && book.getAuthor().equals(author)) {
                    return index;
                }
                currentLine = raf.readLine();
                index = raf.getFilePointer();
            }
            close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Your book was not found!");
        }
        return -1;
    }

    public int amountOfBooksByAuthor(String author) throws IOException {
        Book book;
        int amount = 0;
        try {
            startOfFile();
            String currentLine = raf.readLine();
            while (currentLine != null) {
                book = read(currentLine);
                if (book.isActive() && book.getAuthor().equals(author)) {
                    amount++;
                }
                currentLine = raf.readLine();
            }
            close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return amount;
    }

    public int amountOfBooksByPublisher(String publisher) throws IOException {
        Book book;
        int amount = 0;
        try {
            startOfFile();
            String currentLine = raf.readLine();
            while (currentLine != null) {
                book = read(currentLine);
                if (book.isActive() && book.getPublisher().equals(publisher)) {
                    amount++;
                }
                currentLine = raf.readLine();
            }
            close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return amount;
    }

    public void listBooks() {
        Book book;
        try {
            startOfFile();
            String currentLine = raf.readLine();
            while(currentLine != null) {
                book = read(currentLine);
                if(book != null && book.isActive()) {
                    System.out.println(book.toString());
                }
                currentLine = raf.readLine();
            }
            close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
