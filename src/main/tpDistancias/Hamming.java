package main.tpDistancias;

/**
 * Created by Matias Boracchia & Franco Palumbo on 19-Mar-17.
 */
public class Hamming {

    public static void main(String[] args) {
        String hola = "hola";
        String soga = "soga";

        System.out.println(getHammingDistance(hola, soga)); //prints 2
    }

    /**
     * Gives the Hamming distance, comparing two Strings of the same length letter by letter, starting from the first
     * letter till the last one. If the letters are different adds one and the Hamming distance will be the sum
     * of all the times the two words where different.
     *
     * @param s1 String, first word to be compared
     * @param s2 String, second word to be compared
     * @return int, the sum of all the times the two words where different.
     */
    public static int getHammingDistance(String s1, String s2) {
        int returnValue = 0;
        String t1 = s1.toLowerCase();
        String t2 = s2.toLowerCase();
        if (t1.length() != t2.length()) {
            return -1;
        }
        for (int i = 0; i < t1.length(); i++) {
            if ((int) t1.charAt(i) != (int) t2.charAt(i)) {
                returnValue++;
            }
        }

        return returnValue;
    }

}