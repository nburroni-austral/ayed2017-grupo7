package main.tpDistancias;

/**
 * Created by Matias Boracchia & Franco Palumbo on 19-Mar-17.
 */
public class Levenshtein {

    public static void main(String[] args) {

        String hello = "hello";
        String world = "world";

        System.out.println(getLevenshteinDistance(hello, world)); //prints 4

    }

    /**
     * Receives a String that will be set into lowercase so that Upper case and lower case words will be considered as the
     * same word. Then transformes the String into an array of chars.
     *
     * @param s String, will be the String to be transformed into an array of chars
     * @return charChain, is the array of chars.
     */
    public static char[] getCharChain(String s) {
        String t = s.toLowerCase();
        char[] sToChars = t.toCharArray();
        return sToChars;
    }

    /**
     * Receives two arrays of chars that will be used to get the distance of Levenshtein by creating a double array of
     * chars, filling the first column and row with numbers from zero to the length of each word. The matrix is
     * completed by comparing the positions [i-1][j],[i][j-1],[i-1][j-1] and if the letters being compared in that position
     * are equal [i][j] is the minimum number of positions compared, and if the letters are different [i][j] is the
     * minimum number + 1.
     *
     * @param c1 char[], array of chars that is used in the matrix.
     * @param c2 char[], second array of chars that is used in the matrix.
     * @return matrix int[][], the complete matrix where the last position is the Levenshtein distance.
     */
    public static int[][] getLastChar(char[] c1, char[] c2) {
        int[][] matrix = new int[c1.length + 1][c2.length + 1];
        int lastChar = c2.length + 1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                //Completes the first row and column of the matrix with numbers from zero to the String length
                if (i == 0) {
                    matrix[i][j] = j;
                }
                if (j == 0) {
                    matrix[i][j] = i;
                }
                //Part of the matrix that must be completed by the algorithm
                if (j != 0 && i != 0) {
                    if (c1[i - 1] == c2[j - 1]) {
                        matrix[i][j] = matrix[i - 1][j - 1];
                    }

                    if(c1[i-1]!=c2[j-1]){
                        matrix[i][j]=(findMinimum(matrix[i-1][j-1],matrix[i-1][j],matrix[i][j-1])+1);

                    }
                }

            }
        }
        return matrix;
    }

    /**
     * This method is used to find the minimum int when comparing the three positions of the matrix
     *
     * @param c1 int, first number used to find the minimum
     * @param c2 int, second number used to find the minimum
     * @param c3 int, third number used to find the minimum
     * @return c1/c2/c3 int, the minimum number.
     */
    public static int findMinimum(int c1, int c2, int c3) {
        if (c1 <= c2) {
            if (c1 <= c3) {
                return c1;
            }
            return c3;
        }
        if (c2 <= c3) {
            return c2;
        }
        return c3;
    }

    /**
     * Retrieves the last int from the matrix
     *
     * @param s1 String, first word used to find Levenshtein distance.
     * @param s2 String, second word ued to find Levenshtein distance.
     * @return an int that is the Levenshtein distance.
     */
    public static int getLevenshteinDistance(String s1, String s2) {
        char[] c1 = getCharChain(s1);
        char[] c2 = getCharChain(s2);

        int[][] myChar = getLastChar(c1, c2);
        int maxI = s1.length();
        int maxJ = s2.length();
        return myChar[maxI][maxJ];
    }

}