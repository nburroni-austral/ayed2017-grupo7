package main.tpJML;

/**
 * Created by Usuario on 14-Mar-17.
 */
public class Vector2D {

    private int x;
    private int y;
    private static int maxX = 100;
    private static int maxY = 100;
    /*@
        invariant ((0 <= x) && (x <= maxX)) && ((0 <= y) && (y <= maxY))
    @*/

    public Vector2D(/*@ non_null @*/ int x,/*@ non_null @*/ int y) {
        this.x = x;
        this.y = y;
    }

    public /*@ pure @*/ int getX() {
        return x;
    }

    public /*@ pure @*/ int getY() {
        return y;
    }

    /*@
        requires y >= 0 && y <= maxY
        assignable y;
    @*/
    public void setY(/*@ non_null @*/ int y) {
        this.y = y;
    }

    /*@
        requires x >= 0 && x <= maxX
        assignable x;
    @*/
    public void setX(/*@ non_null @*/ int x) {
        this.x = x;
    }

    /*@
        requires vector.getX() > 0 && vector.getY() > 0;
        signals (VectorSumException e) x > 100 || y > 100 && x == \old(x) && y == \old(y) && e.getReason() == X_OR_Y_ABOVE_MAXIMUM_VALUE_ALLOWED
    @*/
    public void sumWithVector(Vector2D vector) {
        x += vector.getX();
        y += vector.getY();
    }

    /*@
        ensures \result >= 0 && Math.pow(\result, 2) == (Math.pow(x, 2) + Math.pow(y, 2));
    @*/
    public double module() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    /*@
        ensures \result >= 0 && \result == (x * vector.getX() + y * vector.getY());
    @*/
    public int multiplyWithVector(Vector2D vector) {
        return x * vector.getX() + y * vector.getY();
    }

}