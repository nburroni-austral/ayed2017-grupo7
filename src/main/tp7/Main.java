package main.tp7;

import struct.impl.binaryTree.BinaryTreeImplementation;

import static main.tp7.ejercicio13.A.*;
import static main.tp7.ejercicio13.B.*;
import static main.tp7.ejercicio13.C.*;
import static main.tp7.ejercicio13.D.*;
import static main.tp7.ejercicio13.E.*;
import static main.tp7.ejercicio14.A.*;
import static main.tp7.ejercicio14.B.*;

/**
 * Created by Matias on 4/10/17.
 */
public class Main {

    public static void main(String[] args) {

        BinaryTreeImplementation<Integer> b1 = new BinaryTreeImplementation<>(3);
        BinaryTreeImplementation<Integer> b2 = new BinaryTreeImplementation<>(1);
        BinaryTreeImplementation<Integer> b3 = new BinaryTreeImplementation<>(4, b1, b2);
        BinaryTreeImplementation<Integer> b4 = new BinaryTreeImplementation<>(6, b3, b1);
        BinaryTreeImplementation<Integer> b5 = new BinaryTreeImplementation<>(7, b3, b1);
        BinaryTreeImplementation<Integer> b6 = new BinaryTreeImplementation<>();
        BinaryTreeImplementation<Integer> b7 = new BinaryTreeImplementation<>(10, b5, b6);

        //Ejercicio 13

        System.out.println("Peso: " + peso(b1));
        System.out.println("Peso: " + peso(b2));
        System.out.println("Peso: " + peso(b3));

        System.out.println("Cantidad de Hojas: " + numberOfLeafs(b1));
        System.out.println("Cantidad de Hojas: " + numberOfLeafs(b2));
        System.out.println("Cantidad de Hojas: " + numberOfLeafs(b3));

        int numberToFind = 3;
        System.out.println("Numero de veces que aparece el número " + numberToFind + ": " + numberAppearances(b1, numberToFind));
        System.out.println("Numero de veces que aparece el número " + numberToFind + ": " + numberAppearances(b7, numberToFind));
        System.out.println("Numero de veces que aparece el número " + numberToFind + ": " + numberAppearances(b4, numberToFind));

        int level = 3;
        System.out.println("Elementos en el nivel " + level + " = " + elementsInLevel(b3, level));
        System.out.println("Elementos en el nivel " + level + " = " + elementsInLevel(b5, level));
        System.out.println("Elementos en el nivel " + level + " = " + elementsInLevel(b7, level));

        System.out.println("Height: " + height(b2));
        System.out.println("Height: " + height(b5));
        System.out.println("Height: " + height(b7));

        //Ejercicio 14

        System.out.println("La suma de los valores es:\t" + sum(b7));
        System.out.println("La suma de los valores es:\t" + sum(b3));
        System.out.println("La suma de los valores es:\t" + sum(b6));

        System.out.println("La suma de los valores multiplos de 3 es:\t" + sumMultipleOfThree(b7));
        System.out.println("La suma de los valores multiplos de 3 es:\t" + sumMultipleOfThree(b3));
        System.out.println("La suma de los valores multiplos de 3 es:\t" + sumMultipleOfThree(b6));

        System.out.println(areEqual(b4,b4));
        System.out.println(areIsomorphic(b4,b5));
        System.out.println(isComplete(b7));
        System.out.println(isFull(b3));
        System.out.println(isStable(b5));
        System.out.println(containsTree(b5, b4));

        printBorder(b2);
        printBorder(b5);
        //printBorder(b7);

        //Mostrar el árbol

        b7.showInorder();
        System.out.println();
        b7.showPreorder();
        System.out.println();
        b7.showPostorder();
        System.out.println();
        //b7.showByLevels();

    }

}
