package main.tp7.ejercicio14;

import struct.impl.binaryTree.BinaryTreeImplementation;
import java.util.ArrayList;

import static main.tp7.ejercicio13.A.peso;
import static main.tp7.ejercicio13.B.numberOfLeafs;
import static main.tp7.ejercicio14.A.sum;

/**
 * Created by Matias on 4/9/17.
 */
public class B {

    /**
     * Method that informs if two binary trees are equal.
     * Two binary trees are equal if they have the same shape, weight and elements contained in the roots and leaves.
     *
     * @param tree1
     * @param tree2
     * @return TRUE if they are equal. Otherwise, false.
     */
    public static boolean areEqual(BinaryTreeImplementation tree1, BinaryTreeImplementation tree2) {
        if (tree1.getTreeRoot() == null && tree2.getTreeRoot() == null) {
            return true;
        } else if (tree1.getTreeRoot().getLeft() == null && tree2.getTreeRoot().getLeft() == null && tree1.getTreeRoot().getRight() != null &&
                tree2.getTreeRoot().getRight() != null) {
            if (tree1.getRoot().equals(tree2.getRoot())) {
                return areEqual(tree1.getRight(), tree2.getRight());
            } else {
                return false;
            }
        } else if (tree1.getTreeRoot().getLeft() != null && tree2.getTreeRoot().getLeft() != null && tree1.getTreeRoot().getRight() == null &&
                tree2.getTreeRoot().getRight() == null) {
            if (tree1.getRoot().equals(tree2.getRoot())) {
                return areEqual(tree1.getLeft(), tree2.getLeft());
            } else {
                return false;
            }
        } else if (tree1.getTreeRoot().getLeft() != null && tree2.getTreeRoot().getLeft() != null && tree1.getTreeRoot().getRight() != null &&
                tree2.getTreeRoot().getRight() != null) {
            if (tree1.getRoot().equals(tree2.getRoot())) {
                return (areEqual(tree1.getLeft(), tree2.getLeft()) && areEqual(tree1.getRight(), tree2.getRight()));
            } else {
                return false;
            }
        } else if (tree1.getTreeRoot().getLeft() == null && tree2.getTreeRoot().getLeft() == null && tree1.getTreeRoot().getRight() == null &&
                tree2.getTreeRoot().getRight() == null) {
            if (tree1.getRoot().equals(tree2.getRoot())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Method that informs if two binary trees are isomorhic.
     * Two binary trees are isomorphic if they have the same shape.
     * The elements contained in the roots and leaves can be different.
     *
     * @param tree1
     * @param tree2
     * @return TRUE if they are isomorphic. Otherwise, false.
     */
    public static boolean areIsomorphic(BinaryTreeImplementation tree1, BinaryTreeImplementation tree2) {
        if (tree1.getTreeRoot() == null && tree2.getTreeRoot() == null) {
            return true;
        } else if (tree1.getTreeRoot().getLeft() == null && tree2.getTreeRoot().getLeft() == null && tree1.getTreeRoot().getRight() != null &&
                tree2.getTreeRoot().getRight() != null) {
            return areIsomorphic(tree1.getRight(), tree2.getRight());
        } else if (tree1.getTreeRoot().getLeft() != null && tree2.getTreeRoot().getLeft() != null && tree1.getTreeRoot().getRight() == null &&
                tree2.getTreeRoot().getRight() == null) {
            return areIsomorphic(tree1.getLeft(), tree2.getLeft());
        } else if (tree1.getTreeRoot().getLeft() != null && tree2.getTreeRoot().getLeft() != null && tree1.getTreeRoot().getRight() != null &&
                tree2.getTreeRoot().getRight() != null) {
            return (areIsomorphic(tree1.getLeft(), tree2.getLeft()) && areEqual(tree1.getRight(), tree2.getRight()));
        } else if (tree1.getTreeRoot().getLeft() == null && tree2.getTreeRoot().getLeft() == null && tree1.getTreeRoot().getRight() == null &&
                tree2.getTreeRoot().getRight() == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method that informs if two binary trees are alike.
     * Two binary trees are alike if they have the same elements contained in the roots and leaves.
     * The shape of both trees can be different from each other.
     *
     * @param tree1
     * @param tree2
     * @return TRUE if they are alike. Otherwise, false.
     */
    public static boolean areAlike(BinaryTreeImplementation<Integer> tree1, BinaryTreeImplementation<Integer> tree2) {
        if (peso(tree1) == peso(tree2) && sum(tree1) == sum(tree2)) {
            return true;
        }
        return false;
    }

    /**
     * Method that informs if a binary tree is complete.
     * A binary tree is complete when all the roots within the tree have both left and right child or no child at all.
     *
     * @param tree
     * @return TRUE if they it is complete. Otherwise, false.
     */
    public static boolean isComplete(BinaryTreeImplementation tree) {
        if (tree.getTreeRoot() == null) {
            return true;
        } else if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() != null) {
            return (isComplete(tree.getLeft()) && isComplete(tree.getRight()));
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() == null) {
            return true;
        }
        return false;
    }

    /**
     * Method that informs if a binary tree is full.
     * A binary tree is full when all the roots within the tree have both left and right child and all the leaves are at the same level
     *
     * @param tree
     * @return TRUE if they it is complete. Otherwise, false.
     */
    public static boolean isFull(BinaryTreeImplementation tree) {
        if (numberOfLeafs(tree) == Math.pow(2, getLevelOfLeaf(tree))) {
            return true;
        }
        return false;
    }

    private static int getLevelOfLeaf(BinaryTreeImplementation tree) {
        int aux = -1;
        BinaryTreeImplementation treeAux = tree;
        while (treeAux.getTreeRoot() != null) {
            treeAux = treeAux.getLeft();
            aux++;
        }
        return aux;
    }

    /**
     * Method that informs if a binary tree of integers is stable.
     * A binary tree is stable when all child elements are smaller than the parent element.
     *
     * @param tree
     * @return TRUE if they it is stable. Otherwise, false.
     */
    public static boolean isStable(BinaryTreeImplementation<Integer> tree) {
        if (tree.isEmpty()) {
            return true;
        } else if (tree.getTreeRoot().getRight() == null && tree.getTreeRoot().getLeft() == null) {
            return true;
        } else if (tree.getTreeRoot().getLeft() != null) {
            return (tree.getRoot() > tree.getTreeRoot().getLeft().getElement()) && isStable(tree.getLeft());
        } else if (tree.getTreeRoot().getRight() != null) {
            return (tree.getRoot() > tree.getTreeRoot().getRight().getElement()) && isStable(tree.getRight());
        }
        return false;
    }

    /**
     * Method that informs if a binary tree is contained within another binary tree.
     *
     * @param bigTree
     * @param smallTree
     * @return TRUE if small tree is contained in big tree. Otherwise, FALSE.
     */
    public static boolean containsTree(BinaryTreeImplementation bigTree, BinaryTreeImplementation smallTree) {
        if (bigTree.getTreeRoot() == null) {
            return false;
        } else if (smallTree.getTreeRoot() == null) {
            return true;
        } else if (areEqual(bigTree, smallTree)) {
            return true;
        }
        return containsTree(bigTree.getLeft(), smallTree) || containsTree(bigTree.getRight(), smallTree);
    }

    /**
     * Method that prints in console all the elements contained within the leaves of a given tree.
     *
     * @param tree
     */
    public static void printBorder(BinaryTreeImplementation tree) {
        ArrayList treeBorder = getBorder(tree);
        System.out.println(treeBorder.toString());
    }

    /**
     * Method that adds all the elements of the leaves of a given tree into an array list.
     * After the list has been filled, it is returned.
     *
     * @param tree
     * @return
     */
    public static ArrayList getBorder(BinaryTreeImplementation tree) {
        return getBorder(tree, new ArrayList());
    }

    private static ArrayList getBorder(BinaryTreeImplementation tree, ArrayList treeBorder) {
        if (tree.getTreeRoot().getRight() == null && tree.getTreeRoot().getLeft() == null) {
            treeBorder.add(tree.getRoot());
        }
        if (tree.getTreeRoot().getRight() != null || tree.getTreeRoot().getLeft() != null) {
            getBorder(tree.getLeft(), treeBorder);
            getBorder(tree.getRight(), treeBorder);
        }
        return treeBorder;
    }

}