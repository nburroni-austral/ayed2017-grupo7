package main.tp7.ejercicio14;

import struct.impl.binaryTree.BinaryTreeImplementation;

/**
 * Created by Matias on 4/9/17.
 */
public class A {

    /**
     * Method that, given a binary tree of integers, sums the elements contained and returns the result
     *
     * @param tree
     * @return
     */
    public static Integer sum(BinaryTreeImplementation<Integer> tree) {
        if (tree.getTreeRoot() == null) {
            return 0;
        } else if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            return tree.getRoot() + sum(tree.getLeft());
        } else if (tree.getTreeRoot().getRight() != null && tree.getTreeRoot().getLeft() == null) {
            return tree.getRoot() + sum(tree.getRight());
        } else {
            return tree.getRoot() + sum(tree.getLeft()) + sum(tree.getRight());
        }
    }

    /**
     * Method that, given a binary tree of integers, sums the elements contained that are multiples of three and returns the result
     *
     * @param tree
     * @return
     */
    public static Integer sumMultipleOfThree(BinaryTreeImplementation<Integer> tree) {
        if (tree.getTreeRoot() == null) {
            return 0;
        } else if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            if (tree.getRoot() % 3 == 0) {
                return tree.getRoot() + sumMultipleOfThree(tree.getLeft());
            } else {
                return sumMultipleOfThree(tree.getLeft());
            }
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getLeft() != null) {
            if (tree.getRoot() % 3 == 0) {
                return tree.getRoot() + sumMultipleOfThree(tree.getRight());
            } else {
                return sumMultipleOfThree(tree.getRight());
            }
        } else {
            if (tree.getRoot() % 3 == 0) {
                return tree.getRoot() + sumMultipleOfThree(tree.getRight()) + sumMultipleOfThree(tree.getLeft());
            } else {
                return sumMultipleOfThree(tree.getRight()) + sumMultipleOfThree(tree.getLeft());
            }
        }
    }

}