package main.tp7.ejercicio13;

import struct.impl.binaryTree.BinaryTreeImplementation;

/**
 * Created by Matias on 4/9/17.
 */
public class C {

    /**
     * Method that returns the amount of times a given number is repeated inside a binary tree of integers
     *
     * @param tree
     * @param number
     * @return
     */
    public static int numberAppearances(BinaryTreeImplementation<Integer> tree, Integer number) {
        if (tree.getTreeRoot() == null) {
            return 0;
        }
        if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            if ((int) tree.getTreeRoot().getElement() == number) {
                return 1 + numberAppearances(tree.getLeft(), number);
            } else {
                return numberAppearances(tree.getLeft(), number);
            }
        }
        if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() != null) {
            if ((int) tree.getTreeRoot().getElement() == number) {
                return 1 + numberAppearances(tree.getRight(), number);
            } else {
                return numberAppearances(tree.getRight(), number);
            }
        } else {
            if ((int) tree.getTreeRoot().getElement() == number) {
                return 1 + numberAppearances(tree.getLeft(), number) + numberAppearances(tree.getRight(), number);
            } else {
                return numberAppearances(tree.getLeft(), number) + numberAppearances(tree.getRight(), number);
            }
        }
    }

}