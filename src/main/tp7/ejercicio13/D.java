package main.tp7.ejercicio13;

import struct.impl.binaryTree.BinaryTreeImplementation;

/**
 * Created by Matias on 4/9/17.
 */
public class D {

    /**
     * Method that calculates the amount of elements in a given level of a given binary tree.
     *
     * @param tree
     * @param level
     * @return
     */
    public static int elementsInLevel(BinaryTreeImplementation tree, int level) {
        if (level < 0) {
            System.out.println("Level can´t be lower than zero");
            return 0;
        }
        if (tree.getTreeRoot() == null) {
            return 0;
        }
        if (level == 0 && tree.getRoot() != null) {
            return 1;
        }
        if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            int aux = level - 1;
            return elementsInLevel(tree.getLeft(), aux);
        }
        if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() != null) {
            int aux = level - 1;
            return elementsInLevel(tree.getRight(), aux);
        } else {          //(tree.getRoot().getLeft() != null && tree.getRoot().getRight() != null){
            int aux = level - 1;
            return elementsInLevel(tree.getRight(), aux) + elementsInLevel(tree.getLeft(), aux);
        }
    }

}