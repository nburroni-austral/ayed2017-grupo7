package main.tp7.ejercicio13;

import struct.impl.binaryTree.BinaryTreeImplementation;

/**
 * Created by Matias on 4/9/17.
 */
public class E {

    /**
     * Method that returns the height of a given binary tree.
     *
     * @param tree
     * @return
     */
    public static int height(BinaryTreeImplementation tree) {
        if (tree.getTreeRoot() == null) {
            return 0;
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() == null) {
            return 1;
        } else if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            return height(tree.getLeft()) + 1;
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() != null) {
            return height(tree.getRight()) + 1;
        } else { //if(tree.getRoot().getLeft() != null && tree.getRoot().getLeft() != null)
            int a = height(tree.getLeft()) + 1;
            int b = height(tree.getRight()) + 1;
            if (a <= b) {
                return b;
            } else {
                return a;
            }
        }
    }

}