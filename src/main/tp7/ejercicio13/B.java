package main.tp7.ejercicio13;

import struct.impl.binaryTree.BinaryTreeImplementation;

/**
 * Created by Matias on 4/9/17.
 */
public class B {

    /**
     * Method that calculates and returns the amount of leaves of a given binary tree.
     * A leaf of a tree is a root that has no left and no right child.
     *
     * @param tree
     * @return
     */
    public static int numberOfLeafs(BinaryTreeImplementation tree) {
        if (tree.getTreeRoot() == null) {
            return 0;
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() == null) {
            return 1;
        } else if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            return numberOfLeafs(tree.getLeft());
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() != null) {
            return numberOfLeafs(tree.getRight());
        } else {
            return numberOfLeafs(tree.getLeft()) + numberOfLeafs(tree.getRight());
        }
    }

}