package main.tp7.ejercicio13;

import struct.impl.binaryTree.BinaryTreeImplementation;

/**
 * Created by Matias on 4/9/17.
 */
public class A {

    /**
     * Method that calculates and returns the weight of a given binary tree.
     * The weight of a tree is the amount of elements or roots and leaves that are inside the tree.
     *
     * @param tree
     * @return
     */
    public static int peso(BinaryTreeImplementation tree) {
        if (tree.getTreeRoot() == null) {
            return 0;
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() == null) {
            return 1;
        } else if (tree.getTreeRoot().getLeft() != null && tree.getTreeRoot().getRight() == null) {
            return (1 + peso(tree.getLeft()));
        } else if (tree.getTreeRoot().getLeft() == null && tree.getTreeRoot().getRight() != null) {
            return (1 + peso(tree.getRight()));
        } else {
            return 1 + peso(tree.getLeft()) + peso(tree.getRight());
        }
    }

}