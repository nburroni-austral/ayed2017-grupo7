package main.tp4.ejercicio2;

import struct.impl.queue.DynamicQueue;

import java.util.ArrayList;
import java.util.TimerTask;

/**
 * Created by Franco Palumbo on 11-Apr-17.
 */
public class BankQueueB extends TimerTask {

    private ArrayList<DynamicQueue<Client>> checkersQueues;
    //private int[] peopleInChecker = new int[3];                 //peopleByCarrier
    private static Checker[] checkers = new Checker[3];
    private long openTime;
    private int emptyCheckers;
    private long totalWaitingTime;
    private int clientCount = 0;
    private int peopleAtTheMoment;

    /**
     * Method that instantiates a Bank Queue with three checkers, each with its own client queue.
     */
    public BankQueueB() {
        //three checkers and three queues
        checkersQueues = new ArrayList<>();
        checkers[0] = new Checker(0);
        checkers[1] = new Checker(1);
        checkers[2] = new Checker(2);
        openTime = System.currentTimeMillis();
        for (int i = 0; i < 3; i++) {
            checkersQueues.add(new DynamicQueue<>());
        }
        peopleAtTheMoment = 0;
    }

    public void run() {
        //1800 is the 5 hours the bank is open

        entry();
    }

    /**
     * Method that adds up to ten people to the queues after choosing the smallest one.
     */
    public void entry() {
        //Check if Queues are empty and assign a queue for the client
        //Tanda de personas entre 1 y 10
        if (peopleAtTheMoment <= 8 && peopleAtTheMoment >= 4) {
            float random = (float) Math.random();
            if (random >= 0.75) {
                //Client left
                return;
            }
        } else if (peopleAtTheMoment > 8) {
            float random = (float) Math.random();
            if (random >= 0.5) {
                //Client left
                return;
            }
        } else {
            int emptyQueue = checkEmptyQueue();
            int aux = (int) (Math.random() * 10 + 1);
            while (aux > 0) {
                clientCount++;
                peopleAtTheMoment++;
                int queueToGo = goToMinimumQueue(checkersQueues.get(0).size(), checkersQueues.get(1).size(), checkersQueues.get(2).size());
                checkersQueues.get(queueToGo).enqueue(new Client());
                System.out.println("Client entering number: " + clientCount);
                aux--;
            }
        }
    }

    /**
     * Method that calculates the amount of time a client has been waiting in the queue.
     */
    public void attend() {
        // Does the same for each Checker
        for (int i = 0; i < 3; i++) {
            if (checkersQueues.get(i).size() != 0 && !checkers[i].isBusy()) {
                System.out.println("Queue " + (i + 1) + " has " + checkersQueues.get(i).size() + " client/s");
                totalWaitingTime += (checkers[i].attendAClient() - checkersQueues.get(i).dequeue().getTime());
                //peopleInChecker[i]--;
                peopleAtTheMoment--;
                break;
            }
        }
    }

    /**
     * Method that returns the index of the queue that is empty.
     *
     * @return
     */
    public int checkEmptyQueue() {
        int aux = 0;
        for (int i = 0; i < 3; i++) {
            if (checkersQueues.get(i).isEmpty()) {
                aux++;
            }
        }
        return aux;
    }

    /**
     * Method that returns the index of the smallest queue.
     * If two or more queues have the same amount of clients queued, then chooses one randomly
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    private int goToMinimumQueue(int a, int b, int c) {
        int randomForTwoEquals = (int) (Math.random() * 10);
        int randomForThreeEquals = (int) (Math.random() * 3);
        if (a < b && a < c) {
            return 0;
        } else if (b < a && b < c) {
            return 1;
        } else if (c < a && c < b) {
            return 2;
        } else if (a == b && a == c) {
            return randomForThreeEquals;
        } else if (c == a) {
            if (randomForTwoEquals <= 4) {
                return 0;
            } else {
                return 2;
            }
        } else if (c == b) {
            if (randomForTwoEquals <= 4) {
                return 1;
            } else {
                return 2;
            }
        } else if (a == b) {
            if (randomForTwoEquals <= 4) {
                return 0;
            } else {
                return 1;
            }
        }
        System.out.println("No tenias que llegar aca");
        return 5;
    }

    public long getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public long getOpenTime() {
        return openTime;
    }

    public int getClientCount() {
        return clientCount;
    }

}
