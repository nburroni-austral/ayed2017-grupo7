package main.tp4.ejercicio2;

import main.tp4.ejercicio2.BankQueueA;
import main.tp4.ejercicio2.BankQueueB;

import java.util.Timer;

/**
 * Created by Franco Palumbo on 12-Apr-17.
 */
public class Main {

    public static void main(String[] args) {

        //Insantiates a bank from option A
        BankQueueA bank1 = new BankQueueA();

        //Instantiates a timer, that will be in charge of entring people until bank closes
        Timer timer1 = new Timer();
        //Schedules the timer to 90 milisegs. BankOptionA extends from TimerTask, so it has the run() method
        timer1.schedule(bank1, 0, 90);

        //While bank is open, people enter
        while (System.currentTimeMillis() - bank1.getOpenTime() <= (18000)) {
            bank1.attend();
        }

        //Bank is closed, no more people get in
        timer1.cancel();
        timer1.purge();
        System.out.println("The bank was closed");

        //Attends the people that were in the bank before it closed
        while(bank1.getPeopleAtTheMoment()!=0) {
            bank1.attend();
        }

        //Prints the average waiting time
        System.out.println("WAITING TIME "+ bank1.getTotalWaitingTime()/ bank1.getClientCount());
        System.out.println("ATTENDED "+bank1.getClientCount());
        System.out.println("TOTAL WAITING TIME "+bank1.getTotalWaitingTime());

        BankQueueB bank2 = new BankQueueB();
        //Instantiates a timer, that will be in charge of entring people until bank closes
        Timer timer = new Timer();
        //Schedules the timer to 90 milisegs. BankOptionA extends from TimerTask, so it has the run() method
        timer.schedule(bank2, 0, 90);

        //While bank is open, people enter
        while (System.currentTimeMillis() - bank2.getOpenTime() <= (18000)) {
            bank2.attend();
        }

        //Bank is closed, no more people get in
        timer.cancel();
        timer.purge();
        System.out.println("The bank is closed");

        //Attends the people that were in the bank before it closed
        while(bank2.checkEmptyQueue()!=3) {
            bank2.attend();
        }

        System.out.println("WAITING TIME "+ bank2.getTotalWaitingTime()/ bank2.getClientCount());
        System.out.println("ATTENDED "+bank2.getClientCount());
        System.out.println("TOTLA WAITING"+bank2.getTotalWaitingTime());

    }

}
