package main.tp4.ejercicio2;

/**
 * Created by Franco Palumbo on 11-Apr-17.
 */
public class Checker {

    private boolean busy;
    private int attendTime;
    private int id;

    public Checker(int id) {
        busy = false;
        this.id = id;
        switch (id) {
            case 1:
                attendTime = 60;
                break;
            case 2:
                attendTime = 90;
                break;
            case 3:
                attendTime = 120;
                break;
            default:
                attendTime = 90;
        }
    }

    public long attendAClient() {
        System.out.println("Attending checker number: " + id);
        busy = true;
        long ong = System.currentTimeMillis();

        while (busy) {
            if ((System.currentTimeMillis() - ong) >= attendTime) {
                busy = false;
            }
        }
        return System.currentTimeMillis();
    }

    public boolean isBusy() {
        return busy;
    }

    public int getId() {
        return id;
    }

}