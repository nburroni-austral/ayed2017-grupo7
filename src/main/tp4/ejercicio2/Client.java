package main.tp4.ejercicio2;

/**
 * Created by Franco Palumbo on 11-Apr-17.
 */
public class Client {

    private long time;

    public Client() {
        time = System.currentTimeMillis();
    }

    public long getTime() {
        return time;
    }

    public void setTime(long ong) {
        time = ong;
    }

}