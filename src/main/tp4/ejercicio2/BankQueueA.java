package main.tp4.ejercicio2;

import struct.impl.queue.DynamicQueue;

import java.util.TimerTask;

/**
 * Created by Franco Palumbo on 11-Apr-17.
 */
public class BankQueueA extends TimerTask {

    private int peopleAtTheMoment;
    private DynamicQueue<Client> queue = new DynamicQueue<>();
    private long clientCount = 0;
    private long totalWaitingTime;
    private long openTime;
    private static Checker[] checkers = new Checker[3];

    /**
     * Method that instantiates a BankQueue with three checkers and a single queue.
     */
    public BankQueueA() {
        //Instantiates the 3 checkers
        checkers[0] = new Checker(1);
        checkers[1] = new Checker(2);
        checkers[2] = new Checker(3);
        //peopleAtTheMoment = 0;
        //Opens the bank and stores the time
        openTime = System.currentTimeMillis();
    }

    public void run() {
        //18000 is the 5 hours the bank is open
        if (System.currentTimeMillis() - openTime >= 18000) cancel();
        entry();
    }

    /**
     * Method that adds up to 10 people to the queue based on the amount of people that are already queued.
     */
    public void entry() {
        //probability of rehuse
        if (peopleAtTheMoment <= 8 && peopleAtTheMoment >= 4) {
            float random = (float) Math.random();
            if (random >= 0.75) {
                //Client left
                return;
            }
        } else if (peopleAtTheMoment > 8) {
            float random = (float) Math.random();
            if (random >= 0.5) {
                //Client left
                return;
            }
        } else {
            //Tanda de personas entre 1 y 10
            for (int i = 0; i < (int) (Math.random() * 10 + 1); i++) {
                queue.enqueue(new Client());
                peopleAtTheMoment++;
                clientCount++;
                System.out.println("Client entering number: " + clientCount);
            }
        }
    }

    /**
     * Method that randomly chooses a free checker to atend a client and dequeues them from the queue
     */
    public void attend() {
        //checks people in line
        if (peopleAtTheMoment > 0) {
            System.out.println("People in line: " + peopleAtTheMoment);
            //Randomizes the carrier that will attend the client, using the Math.random()
            while (true) {
                int aux = (int) (Math.random() * 3);
                if (!checkers[aux].isBusy()) {
                    totalWaitingTime += checkers[aux].attendAClient() - queue.dequeue().getTime();
                    peopleAtTheMoment--;
                    break;
                }
            }
        }
        return;
    }

    public long getOpenTime() {
        return openTime;
    }

    public long getClientCount() {
        return clientCount;
    }

    public long getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public DynamicQueue<Client> getQueue() {
        return queue;
    }

    public int getPeopleAtTheMoment() {
        return peopleAtTheMoment;
    }

}