package main.tp4.ejercicio3;

import struct.impl.queue.DynamicQueue;

/**
 * Created by Matias on 4/12/17.
 *
 * Una Cola de Prioridad es un tipo de datos el cual consiste de un arreglo,
 * cuyos elementos son punteros a colas, y un entero que determina el tamaño del arreglo.
 * Mientras más grande es el índice del puntero, menor es la prioridad de la cola.
 */
public class PriorityQueue<T> {

    private DynamicQueue<T>[] priorityQueue;
    private int priorityLevels;

    /**
     * Construye una PriorityQueue con un arreglo de tamaño int priorityLevels de Queues vacías
     *
     * @param priorityLevels
     */
    public PriorityQueue(int priorityLevels) {
        priorityQueue = new DynamicQueue[priorityLevels];
        for (int i = 0; i < priorityLevels; i++) {
            priorityQueue[i] = new DynamicQueue<>();
        }
        this.priorityLevels = priorityLevels;
    }

    /**
     * Agrega una Queue q con un nivel de prioridad int index al arreglo de Queues de priorityQueue
     *
     * @param q
     * @param index
     */
    public void addQueue(DynamicQueue<T> q, int index) {
        if (index >= 0 && index < priorityLevels) {
            if (priorityQueue[index] == null) {
                priorityQueue[index] = q;
            } else {
                System.out.println("Priority Queue already has a queue at this priority level");
            }
        } else {
            System.out.println("Index out of bounds");
        }
    }

    /**
     * Elimina y retorna una Queue con un nivel de prioridad int index del arreglo de Queues de priorityQueue
     *
     * @param index
     * @return
     */
    public DynamicQueue<T> removeQueue(int index) {
        DynamicQueue<T> aux = getQueue(index);
        priorityQueue[index] = null;
        return aux;
    }

    /**
     * Agrega el elemento <T> en el índice priorityLevel de la priorityQueue
     *
     * @param t
     * @param priorityLevel
     */
    public void enqueueWithPriority(T t, int priorityLevel) {
        if (priorityLevel >= 0 && priorityLevel < priorityLevels) {
            priorityQueue[priorityLevel].enqueue(t);
        } else {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
    }

    /**
     * Agrega el elemento <T> en la cola de más alta prioridad de priorityQueue
     *
     * @param t
     */
    public void enqueueHighestPriority(T t) {
        priorityQueue[0].enqueue(t);
    }

    /**
     * Elimina y retorna el elemento <T> en el índice priorityLevel de la priorityQueue
     *
     * @param priorityLevel
     * @return
     */
    public T dequeueWithPriority(int priorityLevel) {
        if (priorityLevel >= 0 && priorityLevel < priorityLevels) {
            return priorityQueue[priorityLevel].dequeue();
        } else {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
    }

    /**
     * Elimina y retorna el elemento <T> de la cola de más alta prioridad de priorityQueue
     *
     * @return
     */
    public T dequeueHighestPriority() {
        if (!isEmpty()) {
            for(int i = 0; i < priorityQueue.length; i++) {
                if(!priorityQueue[i].isEmpty()) {
                    return priorityQueue[i].dequeue();
                }
            }
            return null;
        } else {
            throw new RuntimeException("All queues in priority queue are empty");
        }
    }

    /**
     * Informa si PriorityQueue priorityQueue está vacía
     *
     * @return
     */
    public boolean isEmpty() {
        for (int i = 0; i < priorityQueue.length; i++) {
            if (!isPriorityLevelEmpty(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Informa si la queue q dentro de priorityQueue en el int index está vacía
     *
     * @param index
     * @return
     */
    public boolean isPriorityLevelEmpty(int index) {
        if (index >= 0 && index < priorityLevels) {
            return priorityQueue[index].isEmpty();
        } else {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
    }

    /**
     * Retorna la queue de priorityQueue en el índice dado
     *
     * @param index
     * @return
     */
    public DynamicQueue<T> getQueue(int index) {
        if (index >= 0 && index < priorityLevels) {
            return priorityQueue[index];
        } else {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
    }

    /**
     * Informa el tamaño de priorityQueue
     *
     * @return
     */
    public int getPriorityLevels() {
        return priorityLevels;
    }

}
