package main.tp4.ejercicio1;

import struct.impl.stack.DynamicStack;
import struct.impl.queue.DynamicQueue;
import struct.impl.queue.StaticQueue;

import java.util.Scanner;

/**
 * Created by Franco Palumbo on 11-Apr-17.
 */
public class Palindrome {

    public static void main(String[] args) {

        String s = yourWordToCheck();

        System.out.println("Es la palabra: " + s + " palindromo? " + isPalindromeDynamic(s));
        System.out.println("Es la palabra: " + s + " palindromo? " + isPalindromeStatic(s));

    }

    /**
     * Method that accepts input from the user and returns it as a String in lower cases so that it can be compared.
     *
     * @return
     */
    public static String yourWordToCheck() {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Ingrese una palabra para ver si es palindromo");
        String returnValue = myScanner.next();
        return returnValue.toLowerCase();
    }

    /**
     * Method that stores a String in a dynamic queue and a stack, char by char.
     * Then dequeues and pops comparing each return value to determine if a String is a palindrome
     *
     * @param s
     * @return
     */
    public static boolean isPalindromeDynamic(String s) {
        DynamicQueue<Character> queue = new DynamicQueue<>();
        DynamicStack<Character> stack = new DynamicStack<>();
        for (int i = 0; i < s.length(); i++) {
            queue.enqueue(s.charAt(i));
            stack.push(s.charAt(i));
        }
        while (queue.size() != 0) {
            if (queue.dequeue().equals(stack.peek())) {
                stack.pop();
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Method that stores a String in a static queue and a stack, char by char.
     * Then dequeues and pops comparing each return value to determine if a String is a palindrome
     *
     * @param s
     * @return
     */
    public static boolean isPalindromeStatic(String s) {
        DynamicStack<Character> stack = new DynamicStack<>();
        StaticQueue<Character> queue = new StaticQueue<>(s.length());
        for (int i = 0; i < s.length(); i++) {
            queue.enqueue(s.charAt(i));
            stack.push(s.charAt(i));
        }
        while (queue.size() != 0) {
            if (queue.dequeue().equals(stack.peek())) {
                stack.pop();
            } else {
                return false;
            }
        }
        return true;
    }

}