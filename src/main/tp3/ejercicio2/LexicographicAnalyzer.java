package main.tp3.ejercicio2;

import struct.impl.stack.DynamicStack;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Matias on 3/29/17.
 */
public class LexicographicAnalyzer {

    public static void main(String[] args) {

        try {
            charAnalyzer(readFile("src/main/tp3/ejercicio2/LexicographicCorrect.txt"));
            charAnalyzer(readFile("src/main/tp3/ejercicio2/LexicographicWrong.txt"));
        }

        catch (IOException e) {
             System.out.println("File not found");
        }

    }

    /**
     * Method that analyzes a String.
     * If there is a difference in the amount of closing statements and opening statements, prints an error message.
     * If there is no difference, prints a success message
     *
     * @param s String to be analyzed
     */
    private static void charAnalyzer(String s) {

        char[] charArray = s.toCharArray();
        DynamicStack<Character> openingCharacters = new DynamicStack<>();
        DynamicStack<Character> closingCharacters = new DynamicStack<>();

        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == '{' || charArray[i] == '[' || charArray[i] == '(') {
                openingCharacters.push(charArray[i]);
            } else if (charArray[i] == '}' || charArray[i] == ']' || charArray[i] == ')') {
                closingCharacters.push(charArray[i]);
            }
        }

        //closingCharacters = invertStack(closingCharacters);

        if (openingCharacters.size() > closingCharacters.size()) {
            System.out.println("There are more opening statements than closing statements in the code");
            return;
        } else if (openingCharacters.size() < closingCharacters.size()) {
            System.out.println("There are more closing statements than opening statements in the code");
            return;
        }

        int[] amountOfOpeningCharacters = getAmountOfChars(openingCharacters);
        int[] amountOfClosingCharacters = getAmountOfChars(closingCharacters);

        if (amountOfOpeningCharacters[0] > amountOfClosingCharacters[0]) {
            System.out.println("There are more opening '{' statements than closing ones");
        } else if (amountOfOpeningCharacters[0] < amountOfClosingCharacters[0]) {
            System.out.println("There are more closing '}' statements than opening ones");
        } else if (amountOfOpeningCharacters[1] > amountOfClosingCharacters[1]) {
            System.out.println("There are more opening '[' statements than closing ones");
        } else if (amountOfOpeningCharacters[0] < amountOfClosingCharacters[0]) {
            System.out.println("There are more closing ']' statements than opening ones");
        } else if (amountOfOpeningCharacters[2] > amountOfClosingCharacters[2]) {
            System.out.println("There are more opening '(' statements than closing ones");
        } else if (amountOfOpeningCharacters[0] < amountOfClosingCharacters[0]) {
            System.out.println("There are more closing ')' statements than opening ones");
        } else {
            System.out.println("There is no mistake in the code");
        }

        /*
        while(openingCharacters.amountOfInsertions() > 0) {
            if(charClassifier(openingCharacters.peek()) != -charClassifier(closingCharacters.peek())) {
                System.out.println("There is a mistake in the code");
                System.out.println("Expecting closing statement for:\t" + openingCharacters.peek());
                System.out.println("Instead found:\t" + closingCharacters.peek());
                return;
            } else {
                openingCharacters.pop();
                closingCharacters.pop();
            }
        }
        */

    }

    /**
     * Method that returns an array of ints with a length of 3.
     * At array[0], saves the amount of '{' & '}' chars;
     * At array[1], saves the amount of '[' & ']' chars;
     * At array[2], saves the amount of '(' & ')' chars;
     *
     * @param characterStack
     * @return
     */
    private static int[] getAmountOfChars(DynamicStack<Character> characterStack) {
        int[] amountOfCharacters = new int[3];
        DynamicStack<Character> auxStack = characterStack;
        while (auxStack.size() > 0) {
            if(auxStack.peek() == '{' || auxStack.peek() == '}') {
                amountOfCharacters[0]++;
                auxStack.pop();
            } else if (auxStack.peek() == '[' || auxStack.peek() == ']') {
                amountOfCharacters[1]++;
                auxStack.pop();
            } else if (auxStack.peek() == '(' || auxStack.peek() == ')') {
                amountOfCharacters[2]++;
                auxStack.pop();
            } else {
                auxStack.pop();
            }
        }
        return amountOfCharacters;
    }

    /*
    private static DynamicStack<Character> invertStack(DynamicStack<Character> stackToInvert) {
        DynamicStack<Character> auxStack = new DynamicStack<>();
        while (stackToInvert.amountOfInsertions() > 0) {
            auxStack.push(stackToInvert.peek());
            stackToInvert.pop();
        }
        return auxStack;
    }
    */

    private static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

}
