package main.tp3.ejercicio4;

import struct.impl.stack.DynamicStack;

import java.util.Scanner;

/**
 * Created by Franco Palumbo on 26-Mar-17.
 */
public class Calculator {
    private String owner;
    private DynamicStack<Double> characters;

    /**
     * Creates a calculator that will have a DynamicStack full of Doubles, with this stack the Calculator will be able
     * to solve the ecuation when it´s possible
     * @param owner,String that tells the name of the Owner of the Calculator
     */
    public Calculator(String owner) {
        characters = new DynamicStack<>();
        this.owner = owner;
    }

    /**
     * Transforms each character of the String into Doubles and then put it int the Stack so the first
     * Double is in the top and the last one in the bottom.
     *
     * @return boolean, true if the characters were transformed and added, false otherwise.
     */
    public boolean addCharacters() {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Add Math Account: ");
        String myCharChain = myScanner.next();
        for (int i = myCharChain.length() - 1; i >= 0; i--) {
            if (classifyChar(myCharChain.charAt(i)) == -1) {
                System.out.println("Invalid character");
                characters.empty();
                return false;
            }
            if (classifyChar(myCharChain.charAt(myCharChain.length() - 1)) != 0) {        //Checks if last char is a number
                System.out.println(myCharChain.charAt(myCharChain.length() - 1));
                System.out.println("Last char must be a number");
                characters.empty();
                return false;
            }
            if ((classifyChar(myCharChain.charAt(0))) != 0) {          //Checks if first char is a number
                System.out.println("First character is not a Number");
                break;
            }
            if (characters.isEmpty()) {
                characters.push((double) (myCharChain.charAt(i)) - 48);         //The first char is a number so we cast it to int then push it
                continue;
            }
            if (classifyChar(myCharChain.charAt(i)) != classifyDouble(((double) characters.getTopNode().getData())) &&         //Different classes
                    (classifyChar(myCharChain.charAt(i))) + classifyDouble(((double) characters.getTopNode().getData())) != 3) {        //Cant be mult or dive followed by a sum or sub
                if (classifyChar(myCharChain.charAt(i)) == 0) {
                    characters.push((double) (myCharChain.charAt(i)) - 48);
                } else {
                    characters.push((double) myCharChain.charAt(i));    //'+','-','*' and '/' are in Ascii
                }
            } else {
                System.out.println("Not an accepted ecuation");
                characters.empty();
                return false;
            }
        }
        return true;
    }

    /**
     * Classifies the char c into 4 groups or classes: if the char is a number, if it is an multiply or division
     * operator, if it is a sum or subtraction operator or if it is none of the other classes.
     *
     * @param c, is the char that I want to classify
     * @return a number that identifies the class of the char c
     */
    public double classifyChar(char c) {        //classify the chars into 4 classes
        double d = (double) c;
        if (d >= '0' && d <= '9') {
            return 0;       //number
        }
        if (d == '/' || d == '*') {
            return 1;   //mul or div
        }
        if (d == '+' || d == '-') {
            return 2;   //sum or sub
        }
        return -1;  //rest
    }

    /**
     * Classifies doubles into 3 classes, if it is a multiplication or division operator, if it is a sum or subtraction operator
     * or if it is a number
     *
     * @param d , double that I want to classify
     * @return double, a number that identifies the class of the double d.
     */
    public double classifyDouble(double d) {
        if (d == '/' || d == '*') {
            return 1;
        }
        if (d == '+' || d == '-') {
            return 2;
        }
        return 0;
    }

    /**
     * Uses the method addCharacters() and then uses those double to solve the equation from left to right
     *
     * @return double, the solve equation
     */
    public double doTheMath() {
        if (addCharacters()) {
            while (characters.size() != 1) {
                if (characters.getTopNode().hasNext()) {
                    if (classifyDouble((double) characters.getTopNode().next.getData()) == 1) {       //MULTIPLY OR DIVISION
                        if ((double) characters.getTopNode().next.getData() == '/') {           //Is a division
                            double a = ((double) characters.getTopNode().getData());
                            double b = ((double) characters.getTopNode().next.next.getData());
                            double c = a / b;
                            changeValues(c);
                        } else {                                                        //Is a multiplication
                            double a = ((double) characters.getTopNode().getData());
                            double b = (double) (characters.getTopNode().next.next.getData());
                            double c = a * b;
                            changeValues(c);
                            continue;
                        }
                    }
                    if (classifyDouble((double) characters.getTopNode().next.getData()) == 2) {       //SUM OR SUBTRACTION
                        if (characters.getTopNode().next.next.next == null) {
                            double a = (double) characters.getTopNode().getData();
                            double b = (double) (characters.getTopNode().next.next.getData());
                            if ((double) characters.getTopNode().next.getData() == '+') {          //Is a sum
                                double c = a + b;
                                changeValues(c);
                                break;
                            } else {                                                          //is a subtraction
                                double c = a - b;
                                changeValues(c);
                                break;
                            }
                        }
                        if (classifyDouble((double) characters.getTopNode().next.next.next.getData()) == 2) {     //next to the sum or sub there is a sum or sub
                            double a = (double) characters.getTopNode().getData();
                            double b = (double) characters.getTopNode().next.next.getData();
                            if ((double) characters.getTopNode().next.getData() == '+') {  //is a sum
                                double c = a + b;
                                changeValues(c);
                                continue;
                            } else {                                                  //is a subtraction
                                double c = a - b;
                                changeValues(c);
                                continue;
                            }

                        }
                        if (classifyDouble((double) characters.getTopNode().next.next.next.getData()) == 1) {      //next to the sum or sub there is a mult or div exampe: 2+3*4
                            double a = (double) characters.getTopNode().next.next.getData();
                            double b = (double) characters.getTopNode().next.next.next.next.getData();
                            if ((double) characters.getTopNode().next.next.next.getData() == '/') {             //is a division
                                double c = a / b;
                                specialCase(c);
                                continue;
                            } else {                                                                      //is a multiplication
                                double c = a * b;
                                specialCase(c);
                                continue;
                            }
                        }
                    }
                }
            }
        } else {
            System.out.println("Try with the correct ecuation");
            doTheMath();
        }
        return (double) characters.getTopNode().getData();
    }

    /**
     * Takes out the part of the equation that where solved and push the answer to it
     *
     * @param c, is a part of the equation that was solved
     */
    public void changeValues(double c) {
        characters.pop();
        characters.pop();
        characters.pop();
        characters.push(c);
    }

    /**
     * When in the equation a multiplication or division must be solved before solving the sum or subtraction, this
     * method solves it and change the values of the characters.
     *
     * @param c double, is the part of the equation that was solved.
     */
    public void specialCase(double c) {
        characters.getTopNode().next.next.setData(characters.getTopNode().getData());
        characters.getTopNode().next.next.next.setData(characters.getTopNode().next.getData());
        characters.getTopNode().next.next.next.next.setData(c);
        characters.pop();
        characters.pop();
    }

}