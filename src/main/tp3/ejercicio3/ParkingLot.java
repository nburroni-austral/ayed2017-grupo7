package main.tp3.ejercicio3;

import struct.impl.stack.StaticStack;

/**
 * Created by Franco Palumbo on 25-Mar-17.
 */
public class ParkingLot {

    private static int PRICE_OF_LEAVING = 5;
    private StaticStack<Car> parkigSpots;
    private int capacity;
    private int endOfTheDayCollection;
    private int totalCollection;

    /**
     * Creates a parking lot where the first car that enters is parked at the end of the parking lot, and the cars that come afterwards
     * will be parked next to each other, until the parking lot is full. To take a car out of the parking lot, all cars that are
     * between the car and the exit must be taken out and then parked again in the parking lot.
     *
     * @param capacity, int that sets the capacity of the parking lot
     */
    public ParkingLot(int capacity) {
        this.capacity = capacity;
        parkigSpots = new StaticStack<>(capacity);
        endOfTheDayCollection = 0;
        totalCollection = 0;
    }

    /**
     * Creates a parking lot with a maximum capacity of 50 cars.
     */
    public ParkingLot() {
        capacity = 50;
        parkigSpots = new StaticStack<Car>(capacity);
        endOfTheDayCollection = 0;
        totalCollection = 0;
    }

    public int getCapacity() {
        return capacity;
    }

    /**
     * Adds a car to the parking lot, next to the last parked car, as long as the parking lot is not full.
     *
     * @param c, car that will be parked in the parking lot.
     */
    public void addCar(Car c) {
        if (this.amountOfCarsIn() == capacity) {
            System.out.println("Cant add more Cars, parking lot is full");
            return;
        }
        parkigSpots.push(c);
    }

    /**
     * Shows the car that is closest to the exit.
     *
     * @return Car that is closest to the exit
     */
    public Car lookFirstCar() {
        return parkigSpots.peek();
    }

    /**
     * Informs if the parking lot is empty or not.
     *
     * @return true if the car stack is empty, false otherwise.
     */
    public boolean isEmpty() {
        return parkigSpots.isEmpty();
    }

    /**
     * Informs how many cars are parked in the parking.
     *
     * @return int, amountOfInsertions of the car stack
     */
    public int amountOfCarsIn() {
        return parkigSpots.size();
    }

    /**
     * Takes all the cars out of the parking without charging them
     */
    public void emptyTheParking() {
        parkigSpots.empty();
    }

    /**
     * Informs the profit made in the day and then puts the profit into the total profit category
     *
     * @return int, the profit made in the day
     */
    public int getEndOfDayCollection() {
        int aux = endOfTheDayCollection;
        totalCollection = totalCollection + endOfTheDayCollection;
        endOfTheDayCollection = 0;
        return aux;
    }

    /**
     * Prints the profit made in the day
     */
    public void endDay() {
        System.out.println("Money made today:\t" + getEndOfDayCollection());
    }

    /**
     * Method that return how much money the parking lot made in its lifetime
     *
     * @return int, profit made in its lifetime
     */
    public int getTotalCollection() {
        return totalCollection;
    }

    /**
     * Takes out a specific car out of the parking lot and charges it the cost of the stay.
     * If the car is not inside the parking lot, an error message is printed.
     *
     * @param c Car to be removed from the parking lot
     */
    public void removeCar(Car c) {
        if (!isCarInParkingLot(c)) {
            System.out.println(c + "is not in parking lot");
            return;
        }
        StaticStack<Car> aux = new StaticStack<>(capacity);
        StaticStack<Car> aux2 = copyAStack();
        while (aux2.peek().compareTo(c) != 0) {
            aux.push(aux2.peek());
            aux2.pop();
        }
        aux2.pop();
        endOfTheDayCollection += PRICE_OF_LEAVING;
        while (aux.size() != 0) {
            aux2.push(aux.peek());
            aux.pop();
        }
        parkigSpots = aux2;
    }

    /**
     * Informs if a car is inside the parking lot.
     *
     * @param c Car to find inside the parking lot.
     * @return true if car stack contains car c. Otherwise, returns false.
     */
    public boolean isCarInParkingLot(Car c) {
        StaticStack<Car> aux2 = copyAStack();
        while (aux2.size() != 0) {
            if (aux2.peek().compareTo(c) == 0) {
                return true;
            }
            aux2.pop();
        }
        return false;
    }

    /**
     * This method duplicates a StaticStack
     *
     * @return
     */
    private StaticStack<Car> copyAStack() {
        StaticStack<Car> aux1 = new StaticStack<>(capacity);
        StaticStack<Car> aux2 = new StaticStack<>(capacity);
        StaticStack<Car> aux3 = new StaticStack<>(capacity);
        StaticStack<Car> aux4 = new StaticStack<>(capacity);
        while (parkigSpots.size() != 0) {
            aux1.push(parkigSpots.peek());
            aux2.push(this.parkigSpots.peek());
            parkigSpots.pop();
        }
        while (aux1.size() != 0) {
            aux3.push(aux1.peek());
            aux4.push(aux2.peek());
            aux1.pop();
            aux2.pop();
        }
        parkigSpots = aux3;
        return aux4;
    }

}