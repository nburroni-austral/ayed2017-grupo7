package main.tp3.ejercicio3;

/**
 * Created by Franco Palumbo on 28-Mar-17.
 */
public class Main {

    public static void main(String[] args) {

        Car car1 = new Car("Red", "ABC000", "Peugeot", "408");
        Car car2 = new Car("Blue", "ABC001", "Nissan", "Tiida");
        Car car3 = new Car("Purple", "ABC002", "VolksWagen", "Amarok");
        Car car4 = new Car("Blue", "ABC003", "VolksWagen", "Golf");
        Car car5 = new Car("Red", "ABC004", "Citroen", "C3");
        Car car6 = new Car("Grey", "ABC005", "Fiat", "500");
        Car car7 = new Car("Blue", "ABC006", "Seat", "Leon");

        ParkingLot myPL = new ParkingLot(5);

        System.out.println("Created a parking lot with: " + myPL.getCapacity() + " lots");
        System.out.println("Is the parking empty? " + myPL.isEmpty());
        System.out.println("added cars");

        myPL.addCar(car1);
        myPL.addCar(car2);
        myPL.addCar(car3);
        myPL.addCar(car4);
        myPL.addCar(car5);
        myPL.addCar(car6);

        System.out.println("Look first car: " + myPL.lookFirstCar());
        System.out.println("Is car6 in the parking? " + myPL.isCarInParkingLot(car6));
        System.out.println("Removed cars");

        myPL.removeCar(car3);
        myPL.removeCar(car1);
        myPL.removeCar(car7);

        System.out.println("amounts of cars in parking lot: " + myPL.amountOfCarsIn());
        System.out.println("The day has ended");
        System.out.println("get end day money: " + myPL.getEndOfDayCollection());
        System.out.println("Total Money:" + myPL.getTotalCollection());
        System.out.println("New day");
        System.out.println("Remove another car");

        myPL.removeCar(car2);

        System.out.println("The day has eneded");
        System.out.println("get end day money: " + myPL.getEndOfDayCollection());
        System.out.println("Total Money:" + myPL.getTotalCollection());

    }

}
