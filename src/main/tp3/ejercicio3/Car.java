package main.tp3.ejercicio3;

/**
 * Created by Franco Palumbo on 25-Mar-17.
 */
public class Car implements Comparable<Car> {

    private String colour;
    private String matricula;
    private String model;
    private String brand;

    /**
     * Creates a car with a given color, model, license plate and brand
     *
     * @param colour
     * @param matricula
     * @param brand
     * @param model
     */
    public Car(String colour, String matricula, String brand, String model) {
        this.colour = colour;
        this.matricula = matricula;
        this.brand = brand;
        this.model = model;
    }

    /**
     * Method that returns a String containing all the information of a car
     *
     * @return
     */
    public String toString() {
        return "Car{" +
                "colour='" + colour + '\'' +
                ", matricula='" + matricula + '\'' +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }

    /**
     * Informs the license plate of a car
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * Informs if a car is equal to another based on the license plate.
     *
     * @param c Car to be compared to
     * @return int 0 if they are equal. Otherwise, returns 1
     */
    public int compareTo(Car c) {
        if (matricula.compareToIgnoreCase(c.matricula) == 0) {
            return 0;
        }
        return 1;
    }

}