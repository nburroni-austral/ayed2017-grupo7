package main.tp3.ejercicio1;

import struct.impl.stack.DynamicStack;
import struct.impl.stack.StaticStack;

/**
 * Created by Franco Palumbo on 28-Mar-17.
 */
public class Main {

    public static void main(String[] args){

        StaticStack<Pen> myStatickStack = new StaticStack<>(2);

        System.out.println("Is Pila empty?");
        System.out.println(myStatickStack.isEmpty());
        System.out.println(myStatickStack.size());

        myStatickStack.push(new Pen("Blue"));
        myStatickStack.push(new Pen("Red"));

        System.out.println("Added 2 Pens to Pila, amountOfInsertions of pila is: ");
        System.out.println(myStatickStack.size());
        System.out.println("Pen on top:");
        System.out.println(myStatickStack.peek());

        myStatickStack.push(new Pen("Green"));

        System.out.println("Added one pen to pila, pen on top is: ");
        System.out.println(myStatickStack.peek());
        System.out.println("Is pila empty?");
        System.out.println(myStatickStack.isEmpty());
        System.out.println("Size of pila?");
        System.out.println(myStatickStack.size());

        myStatickStack.pop();

        System.out.println("Removed a pen, pen on top is?");
        System.out.println(myStatickStack.peek());

        myStatickStack.empty();

        System.out.println("Removed all pens, is my pila empty?");
        System.out.println(myStatickStack.isEmpty());

        DynamicStack<Pen> myDynamicStack = new DynamicStack<>();
        System.out.println("Is pila empty? "+myDynamicStack.isEmpty());
        System.out.println("Add one Pen");

        myDynamicStack.push(new Pen("Red"));

        System.out.println("Size of pila?");
        System.out.println(myDynamicStack.size());
        System.out.println("Pen on top: "+myDynamicStack.peek());
        System.out.println("Is pila empty? "+myDynamicStack.isEmpty());
        System.out.println("Add many Pen");

        myDynamicStack.push(new Pen("Green"));
        myDynamicStack.push(new Pen("Blue"));
        myDynamicStack.push(new Pen("Grey"));
        myDynamicStack.push(new Pen("Black"));

        System.out.println("Size of pila? "+myDynamicStack.size());
        System.out.println("Pen on top: "+myDynamicStack.peek());
        System.out.println("Remove a Pen");

        myDynamicStack.pop();

        System.out.println("Size of pila? " +myDynamicStack.size());
        System.out.println("Pen on top: "+myDynamicStack.peek());
        System.out.println("Remove all Pens");

        myDynamicStack.empty();

        System.out.println("Is pila empty? "+myDynamicStack.isEmpty());
        System.out.println("Pen on top: " +myDynamicStack.peek());

    }

}