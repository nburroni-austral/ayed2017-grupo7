package main.tp3.ejercicio1;

/**
 * Created by Franco Palumbo on 24-Mar-17.
 */
public class Pen {

    private String colour;

    public Pen(String colour) {
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }

    @Override
    public String toString() {
        return "Pen{" +
                "colour='" + colour + '\'' +
                '}';
    }

}