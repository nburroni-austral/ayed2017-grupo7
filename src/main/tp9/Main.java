package main.tp9;

import java.util.Scanner;

/**
 * Created by Matias Boracchia on 25-Jun-17
 */

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String filePath = "src/main/tp9/assets/";

        System.out.print("Enter the address of the text file you would like to analyze: src/main/tp9/assets/");

        filePath += scanner.next();
        filePath += scanner.nextLine();

        FileCorrector fileCorrector = new FileCorrector(filePath);

        System.out.println();
        System.out.println("Enter 1 to add a word to the dictionary");
        System.out.println("Enter 2 to analyze same file");
        System.out.println("Enter 3 to analyze other file");
        System.out.println("Enter 0 to exit");
        System.out.print("\nWhat would you like to do now? ");
        int optionSelected = scanner.nextInt();

        while(optionSelected != 0) {
            if(optionSelected == 1) {
                System.out.print("\nPlease enter the new word: ");
                String newWord = scanner.next().toLowerCase();
                fileCorrector.addWord(newWord);
            }
            if(optionSelected == 2) {
                fileCorrector.find(filePath);
            }
            if(optionSelected == 3) {
                filePath = "src/main/tp9/assets/";
                System.out.print("\nPlease enter the new file path: src/main/tp9/assets/");
                filePath += scanner.next();
                filePath += scanner.nextLine();
                fileCorrector.find(filePath);
            }
            System.out.println();
            System.out.println("Enter 1 to add a word to the dictionary");
            System.out.println("Enter 2 to analyze file once again");
            System.out.println("Enter 3 to change the file path");
            System.out.println("Enter 0 to exit");
            System.out.print("\nWhat would you like to do now? ");
            optionSelected = scanner.nextInt();
        }

    }

}
