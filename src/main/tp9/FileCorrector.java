package main.tp9;

import struct.impl.list.DynamicList;

import java.io.*;

/**
 * Created by Matias Boracchia on 25-Jun-17
 */

public class FileCorrector {

    private Dictionary dictionary;
    private BufferedReader fileReader;
    private DynamicList<Error> words;

    public FileCorrector(String filePath) {
        dictionary = new Dictionary("src/main/tp9/assets/Dictionary.txt");
        words = new DynamicList<>();
        find(filePath);
    }

    void find(String str) {
        try {
            FileReader file = new FileReader(str);
            fileReader = new BufferedReader(file);
            search();
            write();
        } catch (FileNotFoundException e) {
            System.out.println("\nThe file can't was not found or is corrupted");
        }
    }

    void addWord(String word) {
        dictionary.addWord(word);
    }

    private void search() {
        try {
            String line;
            int indexLine = 1;
            while ((line = fileReader.readLine()) != null) {
                searchWord(line, indexLine);
                indexLine++;
            }
        } catch (IOException e) {
            System.out.println("The file could not be read");
        }
    }

    private void searchWord(String phrase, int index) {
        String word = "";
        for (int i = 0; i < phrase.length(); i++) {
            char aux = phrase.charAt(i);
            switch (aux) {
                case ' ':
                    if (!dictionary.containsWord(word.toLowerCase())) {
                        Error error = new Error();
                        error.word = word;
                        error.indexLine = index;
                        words.insertNext(error);
                    }
                    word = "";
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '!':
                case '?':
                case ';':
                case ',':
                case '.':
                    break;
                default:
                    word += aux;
                    break;
            }
        }
    }

    private void write() {
        try {
            FileWriter fileWriter = new FileWriter("src/main/tp9/assets/Corrections.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            for (int i = 0; i < words.size(); i++) {
                words.goTo(i);
                String theWORD = words.getActual().word;
                String phrase = "\nLine " + words.getActual().indexLine + " -> " + theWORD + ": ";
                printWriter.println(phrase);
                DynamicList<String> similarWords = dictionary.similarWords(theWORD);
                if (similarWords != null) {
                    for (int j = 0; j < similarWords.size(); j++) {
                        similarWords.goTo(j);
                        printWriter.println("\t\t\t*" + similarWords.getActual());
                    }
                } else {
                    printWriter.println("\t\t\t*No matches");
                }
            }
            printWriter.close();
            System.out.println("\nYou'll find the mistakes of your text file in the following address: 'src/main/tp9/assets/Corrections.txt'");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class Error {
        String word;
        int indexLine;
    }

}
