package main.tp6;

import struct.istruct.Comparable;

import java.io.Serializable;

/**
 * Created by Franco Palumbo on 13-May-17.
 */
public class Bus implements Comparable<Bus>, Serializable {
    private int lineNumber;
    private int internalNumber;
    private int amountOfSeats;
    private boolean isSuitableForDisable;

    public Bus(int lineNumber, int internalNumber, int amountOfSeats, boolean isSuitableForDisable) {
        this.lineNumber = lineNumber;
        this.internalNumber = internalNumber;
        this.amountOfSeats = amountOfSeats;
        this.isSuitableForDisable = isSuitableForDisable;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int getInternalNumber() {
        return internalNumber;
    }

    public int getAmountOfSeats() {
        return amountOfSeats;
    }

    public boolean isSuitableForDisable() {
        return isSuitableForDisable;
    }

    @Override
    public int compareTo(Comparable<Bus> x) {
        if (this.lineNumber > ((Bus) x).getLineNumber()) {
            return 1;
        } else if (this.lineNumber < ((Bus) x).getLineNumber()) {
            return -1;
        } else if (this.internalNumber > ((Bus) x).getInternalNumber()) {
            return 1;
        } else if (this.internalNumber < ((Bus) x).getInternalNumber()) {
            return -1;
        } else {
            //System.out.println("Two Buses can´t have the same line and internal number");
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Bus{" +
                "lineNumber=" + lineNumber +
                ", internalNumber=" + internalNumber +
                '}';
    }
}
