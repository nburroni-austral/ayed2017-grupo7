package main.tp6;

import struct.istruct.Comparable;
import struct.istruct.list.GeneralList;
import struct.istruct.list.SortedList;

import java.io.Serializable;

/**
 * Created by Franco Palumbo on 13-May-17.
 */
public class DynamicSortedList<T extends Comparable> implements SortedList<T>,Serializable {
    private Node<T> head, window, sentinel;
    private int size;

    public DynamicSortedList() {
        head = new Node<>();
        sentinel = new Node<>();
        head.next = sentinel;
        window = head;
        size = 0;
    }

    public T getActual() {
        return window.obj;
    }

    public int getActualPosition() {
        int pos = 0;
        if (!isVoid()) {
            Node<T> aux = head;
            for (; aux.next != window; pos++) aux = aux.next;
        }
        return pos;
    }

    public boolean isVoid() {
        return head.next == sentinel;
    }

    public boolean endList() {
        return window.next == sentinel;
    }

    public GeneralList<T> clone() {
        return null;
    }

    public void insert(T obj) {
        if (isVoid()) {
            insertNext(obj);
            return;
        }
        while (window.next != sentinel) {
            if ((window.obj).compareTo(obj) > 0) {
                insertPrev(obj);
                window = head.next;
                return;
            } else {
                window = window.next;
            }
        }
        if ((window.obj).compareTo(obj) > 0) {
            insertPrev(obj);
            window = head.next;
        } else {
            insertNext(obj);
            window = head.next;
        }
    }

    public void removeElement(T obj) {
        if (isVoid()) {
            System.out.println("No obj");
            return;
        }
        while (window.next != sentinel) {
            if ((window.obj).compareTo(obj) == 0) {
                remove();
                window = head.next;
                return;
            } else {
                window = window.next;
            }
        }
        if ((window.obj).compareTo(obj) == 0) {
            remove();
            window = head.next;
        } else {
            System.out.println(obj + "was not found in the sorted list");
        }
    }

    public boolean isInList(T obj) {
        if (isVoid()) {
            return false;
        }
        while (window.next != sentinel) {
            if ((window.obj).compareTo(obj) == 0) {
                window = head.next;
                return true;
            } else {
                window = window.next;
            }
        }
        if ((window.obj).compareTo(obj) == 0) {
            window = head.next;
            return true;
        } else {
            return false;
        }
    }

    private void insertPrev(T obj) {
        if (!isVoid()) {
            goBack();
        }
        insertNext(obj);
    }


    private void insertNext(T obj) {
        window.next = new Node<>(obj, window.next);
        window = window.next;
        size++;
    }

    public void remove() {
        if (isVoid()) throw new NullPointerException("This List is empty");
        goBack();
        window.next = window.next.next;
        window = window.next;
        if (window == sentinel) goBack();
        size--;
    }

    public void goNext() {
        if (window.next == sentinel) throw new IndexOutOfBoundsException("Reached the end of this List");
        window = window.next;
    }

    public void goPrev() {
        if (window == head.next) throw new IndexOutOfBoundsException("Reached the beginning of this List");
        goBack();
    }

    private void goBack() {
        Node<T> aux = head;
        while (window != aux.next) {
            aux = aux.next;
        }
        window = aux;
    }

    public void goTo(int index) {
        window = head.next;
        for (int i = 0; i < index; i++) {
            window = window.next;
        }
    }


    public int size() {
        return size;
    }

    private static class Node<E> implements Serializable {
        E obj;
        Node<E> next;

        Node() {
            obj = null;
            next = null;
        }

        Node(E o) {
            obj = o;
            next = null;
        }

        Node(E o, Node<E> next) {
            this(o);
            this.next = next;
        }

        boolean hasNoObj() {
            return obj == null;
        }
    }
}
