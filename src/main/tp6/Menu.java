package main.tp6;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Franco Palumbo on 13-May-17.
 */
public class Menu implements Serializable {

    private DynamicSortedList<Bus> list;

    public Menu() {
        list = new DynamicSortedList<>();
    }

    public void addBus(Bus bus) {
        list.insert(bus);
    }

    public void removeBus(Bus bus) {
        list.removeElement(bus);
    }

    public void showReport() {
        System.out.println("Buses:");
        for (int i = 0; i < list.size(); i++) {
            list.goTo(i);
            System.out.println(list.getActual());
        }
        list.goTo(0);
    }

    public void showSuitableForDisable() {
        int numberOfBusesSuitableForDisables = 0;
        for (int i = 0; i < list.size(); i++) {
            list.goTo(i);
            if (list.getActual().isSuitableForDisable() == true) {
                numberOfBusesSuitableForDisables++;
            }
            if (i == list.size() - 1) {
                System.out.println("Bus Line number: " + list.getActual().getLineNumber() + " has " + numberOfBusesSuitableForDisables + " buses suitable for disable people");
                return;
            }
            int aux = list.getActual().getLineNumber();
            list.goNext();
            if (aux != list.getActual().getLineNumber()) {
                list.goPrev();
                System.out.println("Bus Line number: " + list.getActual().getLineNumber() + " has " + numberOfBusesSuitableForDisables + " buses suitable for disable people");
                numberOfBusesSuitableForDisables = 0;
            }
        }
    }

    public void showMoreThan27Seats() {
        int busesWithMoreThan27Seats = 0;
        for (int i = 0; i < list.size(); i++) {
            list.goTo(i);
            if (list.getActual().getAmountOfSeats() > 27) {
                busesWithMoreThan27Seats++;
            }
            if (i == list.size() - 1) {
                System.out.println("Bus Line number: " + list.getActual().getLineNumber() + " has " + busesWithMoreThan27Seats + " buses with more than 27 seats");
                return;
            }
            int aux = list.getActual().getLineNumber();
            list.goNext();
            if (aux != list.getActual().getLineNumber()) {
                list.goPrev();
                System.out.println("Bus Line number: " + list.getActual().getLineNumber() + " has " + busesWithMoreThan27Seats + " buses with more than 27 seats");
                busesWithMoreThan27Seats = 0;
            }
        }
    }

    public void saveList() throws IOException {
        FileOutputStream exit = new FileOutputStream("ListOfBuses");
        ObjectOutputStream outputStream = new ObjectOutputStream(exit);
        outputStream.writeObject(list);
        outputStream.close();
    }

    public void recoverList() throws IOException, ClassNotFoundException {
        FileInputStream exit = new FileInputStream("ListOfBuses");
        ObjectInputStream inputStream = new ObjectInputStream(exit);
        list = (DynamicSortedList<Bus>) inputStream.readObject();
        inputStream.close();
    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Action to take place: ");
        System.out.println("1 Add Bus, 2 Delete Bus, 3 Show Report, 4 suitable for disable, 5 buses with more than 27 seats, 6 save list, 7 get list ");
        int number = scanner.nextInt();
        while (number != 0) {


            if (number < 0 || number > 7) {
                System.out.println("Not a possible action, try again");
                number = scanner.nextInt();
                continue;
            }
            if (number == 1) {
                System.out.println("Bus to be added: Line of the Bus, Internal number of the Bus, amount of seats, is suitable for disables");
                Bus bus = new Bus(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextBoolean());
                menu.addBus(bus);
            } else if (number == 2) {
                System.out.println("Bus to be removed: Line of the Bus, Internal number of the Bus, amount of seats, is suitable for disables");
                Bus bus = new Bus(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextBoolean());
                menu.removeBus(bus);
            } else if (number == 3) {
                menu.showReport();
            } else if (number == 4) {
                menu.showSuitableForDisable();
            } else if (number == 5) {
                menu.showMoreThan27Seats();
            } else if (number == 6) {
                try {
                    menu.saveList();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                System.out.println("The List was saved");
            } else if (number == 7) {
                try {
                    menu.recoverList();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                System.out.println("The list was recovered");
            }
            System.out.println("New action to take place: ");
            number = scanner.nextInt();
        }
    }
}
