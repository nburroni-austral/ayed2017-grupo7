package main.tp8;

import main.tp8.models.LightBulb;
import main.tp8.models.LightStore;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Matias on 4/24/17.
 */
public class Main {

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);
        LinkedList<LightBulb> oldSystem = new LinkedList<>();

        oldSystem.add(new LightBulb("skjdh".toCharArray(), 234, "LED", 12));
        oldSystem.add(new LightBulb("asdiw".toCharArray(), 564, "LED + RGB", 20));
        oldSystem.add(new LightBulb("weoif".toCharArray(), 456, "LED + RGB", 5));
        oldSystem.add(new LightBulb("aswek".toCharArray(), 480, "LED", 16));
        oldSystem.add(new LightBulb("lrtkg".toCharArray(), 987, "LED", 50));
        oldSystem.add(new LightBulb("irufh".toCharArray(), 342, "LED + RGB", 23));

        LightStore genericCompany = new LightStore(oldSystem, "GenericElectronics INC.");

        System.out.println("Bienvenido a " + genericCompany.getName());

        System.out.println("\n----------------------------------------------------------------");
        System.out.println("Ingrese 1 para agregar un producto");
        System.out.println("Ingrese 2 para eliminar un producto");
        System.out.println("Ingrese 3 para modificar un producto");
        System.out.println("Ingrese 4 para obtener un informe");
        System.out.println("Ingrese 5 para terminar el programa");
        System.out.println("----------------------------------------------------------------\n");
        System.out.print("¿Qué desea hacer?:\t");

        int selection = myScanner.nextInt();

        while(selection != 5) {

            if(selection == 1) {

                System.out.print("\nIngrese el código (5 caracteres) del producto:\t");
                char[] code = myScanner.next().toCharArray();
                System.out.print("Ingrese el tipo del producto:\t");
                String type = myScanner.next();
                System.out.print("Ingrese cuantos watts tiene el producto:\t");
                int watts = myScanner.nextInt();
                System.out.print("Ingrese el stock del producto:\t");
                int stock = myScanner.nextInt();

                LightBulb newLightBulb = new LightBulb(code, watts, type, stock);
                genericCompany.addProduct(newLightBulb);

                System.out.println("\nProducto (" + newLightBulb.getCode() + ") agregado con éxito");

                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para agregar otro producto");
                System.out.println("Ingrese 2 para eliminar un producto");
                System.out.println("Ingrese 3 para modificar un producto");
                System.out.println("Ingrese 4 para obtener un informe");
                System.out.println("Ingrese 5 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");

                selection = myScanner.nextInt();

            } else if(selection == 2) {

                System.out.print("\nIngrese el código (5 caracteres) del producto que desea eliminar:\t");
                String code = myScanner.next();

                LightBulb deletedLightBulb = genericCompany.searchProduct(code, genericCompany.getStorage());
                genericCompany.removeProduct(genericCompany.searchProduct(code, genericCompany.getStorage()));

                System.out.println("\nProducto (" + deletedLightBulb.getCode() + ") eliminado con éxito");

                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para agregar un producto");
                System.out.println("Ingrese 2 para eliminar otro producto");
                System.out.println("Ingrese 3 para modificar un producto");
                System.out.println("Ingrese 4 para obtener un informe");
                System.out.println("Ingrese 5 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();

            } else if(selection == 3) {

                System.out.println("\nIngrese 1 para agregar stock a un producto");
                System.out.println("Ingrese 2 para quitar stock a un producto");
                System.out.println("Ingrese cualquier número para regresar al menú principal\n");
                System.out.print("¿Qué desea hacer?:\t");

                int secondSelection = myScanner.nextInt();

                if(secondSelection == 1) {

                    System.out.print("\nIngrese el código (5 caracteres) del producto al que desea agregar stock:\t");
                    String code = myScanner.next();
                    System.out.print("Hay " + genericCompany.searchProduct(code, genericCompany.getStorage()).getStock() + " productos en stock. Ingrese la cantidad de stock que desea sumar:\t");
                    int amount = myScanner.nextInt();

                    genericCompany.addStock(amount, genericCompany.searchProduct(code, genericCompany.getStorage()));

                } else if (secondSelection == 2) {
                    System.out.print("\nIngrese el código (5 caracteres) del producto al que desea quitar stock:\t");
                    String code = myScanner.next();
                    System.out.print("Hay " + genericCompany.searchProduct(code, genericCompany.getStorage()).getStock() + " productos en stock. Ingrese la cantidad de stock que desea restar:\t");
                    int amount = myScanner.nextInt();

                    genericCompany.removeStock(amount, genericCompany.searchProduct(code, genericCompany.getStorage()));

                }

                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para agregar un producto");
                System.out.println("Ingrese 2 para eliminar un producto");
                System.out.println("Ingrese 3 para modificar otro producto");
                System.out.println("Ingrese 4 para obtener un informe");
                System.out.println("Ingrese 5 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();

            } else if(selection == 4) {

                if(!genericCompany.getStorage().isEmpty()) {
                    genericCompany.printInformation(genericCompany.getStorage());
                } else {
                    System.out.println("No hay ningún producto almacenado");
                }

                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para agregar un producto");
                System.out.println("Ingrese 2 para eliminar un producto");
                System.out.println("Ingrese 3 para modificar un producto");
                System.out.println("Ingrese 4 para obtener otro informe");
                System.out.println("Ingrese 5 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();

            } else {
                System.out.println("\nEsa no es una opción válida!");
                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para agregar un producto");
                System.out.println("Ingrese 2 para eliminar un producto");
                System.out.println("Ingrese 3 para modificar un producto");
                System.out.println("Ingrese 4 para obtener un informe");
                System.out.println("Ingrese 5 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();
            }

        }

    }

}
