package main.tp8.models;

/**
 * Created by Matias on 4/24/17.
 */
public class LightBulb implements Comparable {

    private char[] code;
    private String type;
    private int watts;
    private int stock;

    public LightBulb (char[] code, int watts, String type, int stock) {
        if(code.length == 5) {
            this.code = code;
        }
        if(type.length() > 10) {
            this.type = type.substring(0, 9);
        } else {
            this.type = type;
        }
        this.watts = watts;
        if(stock >= 0) {
            this.stock = stock;
        }
    }

    public LightBulb (char[] code, int watts, String type) {
        if(code.length == 5) {
            this.code = code;
        }
        if(type.length() <= 10) {
            this.type = type;
        }
        this.watts = watts;
        stock = 0;
    }

    public void printInformation() {
        System.out.println("\n-------------------------");
        System.out.print("Bombita de Luz (" + getCode() + ")\n");
        System.out.print("\tTipo:\t" + getType() + "\n");
        System.out.print("\tWatts:\t" + getWatts() + "\n");
        System.out.print("\tStock:\t" + getStock() + "\n");
        System.out.print("-------------------------");
    }

    public String getCode() {
        String codeToString = "";
        for(int i = 0; i < code.length; i++) {
            codeToString += code[i];
        }
        return codeToString;
    }

    public String getType() {
        return type;
    }

    public int getWatts() {
        return watts;
    }

    public int getStock() {
        return stock;
    }

    public void addStock(int amount) {
        if(amount > 0) {
            stock += amount;
        }
    }

    public void removeStock(int amount) {
        if(amount > 0 && amount <= stock) {
            stock -= amount;
        }
    }

    public int codeToInt() {
        int codeToInt = 0;
        for(int i = 0; i < code.length; i++) {
            codeToInt += (int) code[i];
        }
        return codeToInt;
    }

    @Override
    public int compareTo(Object o) {
        LightBulb other = (LightBulb) o;
        return codeToInt() - other.codeToInt();
    }

}
