package main.tp8.models;

import struct.impl.binaryTree.BinarySearchTreeImplementation;

import java.util.LinkedList;

/**
 * Created by Matias on 4/24/17.
 */
public class LightStore {

    private BinarySearchTreeImplementation<LightBulb> storage;
    private String name;

    public LightStore(LinkedList<LightBulb> products, String name) {
        storage = new BinarySearchTreeImplementation<>();
        for(int i = 0; i < products.size(); i++) {
            storage.insert(products.get(i));
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public BinarySearchTreeImplementation<LightBulb> getStorage() {
        return storage;
    }

    public void printInformation(BinarySearchTreeImplementation<LightBulb> t) {
        if(!t.isEmpty()) {
            if(t.getRoot() != null) {
                t.getRoot().printInformation();
            }
            if(t.getLeft() != null) {
                printInformation(t.getLeft());
            }
            if(t.getRight() != null) {
                printInformation(t.getRight());
            }
        }
    }

    public LightBulb searchProduct(String code, BinarySearchTreeImplementation<LightBulb> t) {
        if(t.getRoot().getCode().equals(code)) {
            return t.getRoot();
        } else if(t.getLeft() != null) {
            return searchProduct(code, t.getLeft());
        } else if(t.getRight() != null) {
            return searchProduct(code, t.getRight());
        } else {
            throw new RuntimeException("El producto no existe");
        }
    }

    public void addStock(int amount, LightBulb lightBulb) {
        if(storage.exists(lightBulb)) {
            storage.search(lightBulb).addStock(amount);
        }
    }

    public void removeStock(int amount, LightBulb lightBulb) {
        if(storage.exists(lightBulb)) {
            storage.search(lightBulb).removeStock(amount);
        }
    }

    public void addProduct(LightBulb lightBulb) {
        if(!storage.exists(lightBulb)) {
            storage.insert(lightBulb);
        }
    }

    public void removeProduct(LightBulb lightBulb) {
        if(storage.exists(lightBulb)) {
            storage.delete(lightBulb);
        }
    }

}
