package main.tpEquipos;

/**
 * Created by Franco Palumbo on 15-Apr-17.
 */
public class WinsTiesLosses {

    private int wins;
    private int ties;
    private int losses;

    public WinsTiesLosses(int wins, int ties, int losses) {
        this.wins = wins;
        this.ties = ties;
        this.losses = losses;
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    @Override
    public String
    toString() {
        return
                "wins=" + wins +
                        ", ties=" + ties +
                        ", losses=" + losses +
                        '}';
    }

}