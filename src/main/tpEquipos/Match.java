package main.tpEquipos;

/**
 * Created by Franco Palumbo on 15-Apr-17.
 */
public class Match {

    private Team localTeam;
    private Team visitTeam;
    private char result;

    public Match(Team localTeam, Team visitTeam) {
        this.localTeam = localTeam;
        this.visitTeam = visitTeam;
        result = '0';
    }

    public Team getLocalTeam() {
        return localTeam;
    }

    public Team getVisitTeam() {
        return visitTeam;
    }

    public char getResult() {
        return result;
    }

    public void setResult(char result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Match{" +
                "localTeam='" + localTeam + '\'' +
                ", visitTeam='" + visitTeam + '\'' +
                ", result=" + result +
                '}';
    }

}