package main.tpEquipos;

import java.util.Scanner;

/**
 * Created by Franco Palumbo on 15-Apr-17.
 */
public class Team {

    private String name;
    private int points;
    private int playedMatches;
    private WinsTiesLosses wtl;

    public Team(String name, int points) {
        this.name = name;
        this.points = points;
        playedMatches = 0;
        wtl = new WinsTiesLosses(0, 0, 0);
    }

    public void setPlayedMatches(int playedMatches) {
        this.playedMatches = playedMatches;
    }

    public void setWtl(WinsTiesLosses wtl) {
        this.wtl = wtl;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public int getPlayedMatches() {
        return playedMatches;
    }

    public WinsTiesLosses getWtl() {
        return wtl;
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", points=" + points +
                ", playedMatches=" + playedMatches +
                '}';
    }

}