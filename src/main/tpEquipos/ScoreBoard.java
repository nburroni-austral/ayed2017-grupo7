package main.tpEquipos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Franco Palumbo on 15-Apr-17.
 */

public class ScoreBoard {

    private int teamsInScoreBoard;
    private int matchesPlayed;
    private Team[] teamsPositions;
    private Match[] matches;
    private int[] multiResults;

    public ScoreBoard() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Teams & Matches Played: ");
        String s = scanner.nextLine();
        String[] parts = s.split(" ");
        int aux = Integer.parseInt(parts[0]);
        if (aux == -1) {
            this.teamsInScoreBoard = aux;
            this.matchesPlayed = 0;
            teamsPositions = null;
            matches = null;
            multiResults = null;
            return;
        }
        this.teamsInScoreBoard = aux;
        this.matchesPlayed = Integer.parseInt(parts[1]);
        teamsPositions = new Team[teamsInScoreBoard];
        matches = new Match[matchesPlayed];
        multiResults = new int[matches.length];
        multiResults = new int[matches.length];
        for (int p = 0; p < matches.length; p++) {
            multiResults[p] = 0;
        }
    }

    public int getTeamsInScoreBoard() {
        return teamsInScoreBoard;
    }

    public int getMatchesPlayed() {
        return matchesPlayed;
    }

    public Match[] getMatches() {
        return matches;
    }

    @Override
    public String toString() {
        return "ScoreBoard{" +
                "teamsInScoreBoard=" + teamsInScoreBoard +
                ", matchesPlayed=" + matchesPlayed +
                ", teamsPositions=" + Arrays.toString(teamsPositions) +
                ", matches=" + Arrays.toString(matches) +
                '}';
    }

    /**
     * Gets all the teams and their points from console and saves them in an array of Team
     */
    public void fillTable() {
        Scanner myScanner = new Scanner(System.in);
        for (int i = 0; i < teamsInScoreBoard; i++) {
            String aux = myScanner.nextLine();
            String[] parts = aux.split(" ");
            String name = parts[0];
            int points = Integer.parseInt(parts[1]);
            teamsPositions[i] = new Team(name, points);
        }
    }

    /**
     * Gets all the matches from the console and saves them in an array of Match
     */
    public void fillTheMatches() {
        Scanner myScanner = new Scanner(System.in);
        for (int i = 0; i < matchesPlayed; i++) {
            String aux = myScanner.nextLine();
            String[] parts = aux.split(" ");
            String local = parts[0];
            String visit = parts[1];
            Team localTeam = null;
            Team visitTeam = null;
            for (int j = 0; j < teamsPositions.length; j++) {
                if (teamsPositions[j].getName().equals(local)) {
                    localTeam = teamsPositions[j];
                }
                if (teamsPositions[j].getName().equals(visit)) {
                    visitTeam = teamsPositions[j];
                }

            }
            matches[i] = new Match(localTeam, visitTeam);
            setTeamsMatches(local, visit);
        }
    }

    /**
     * Este metodo privado es usado para agregar el partido jugado a los respectivos equipos, local y visitante
     * Adds a match played to both teams who played a match.
     *
     * @param local, name of the local team
     * @param visit, name of the visit team
     */
    public void setTeamsMatches(String local, String visit) {
        for (int i = 0; i < teamsInScoreBoard; i++) {
            if (teamsPositions[i].getName().toLowerCase().equals(local.toLowerCase()) ||
                    teamsPositions[i].getName().toLowerCase().equals(visit.toLowerCase())) {

                int aux = teamsPositions[i].getPlayedMatches();
                teamsPositions[i].setPlayedMatches(aux + 1);
            }
        }
    }

    /**
     * Metodo en el cual se consigen todos los posibles resultados de WTL de cada equipo
     * Gets all the possible results of a Team (won, tied or lost) games for all Teams based on their points.
     *
     * @return Array of ArrayList of WinsTiesLosses, has the amountOfInsertions of the amaunt of teams, for each team there is one or
     * more possible combinations of a Team´s WinsTiesLosses.
     */
    public ArrayList<ArrayList<WinsTiesLosses>> possibleWTL() {
        ArrayList<ArrayList<WinsTiesLosses>> arrayOfArraysOfWTL = new ArrayList<>(teamsInScoreBoard);
        for (int i = 0; i < teamsInScoreBoard; i++) {
            int residual = teamsPositions[i].getPoints() % 3;
            int matchesPlayed = teamsPositions[i].getPlayedMatches();
            int wins = teamsPositions[i].getPoints() / 3;
            int ties = residual;
            int losses = matchesPlayed - wins - ties;
            arrayOfArraysOfWTL.add(i, new ArrayList<>());
            arrayOfArraysOfWTL.get(i).add(new WinsTiesLosses(wins, ties, losses));

            while (losses >= 2 && wins >= 1) {
                losses = losses - 2;
                wins--;
                ties = ties + 3;
                arrayOfArraysOfWTL.get(i).add(new WinsTiesLosses(wins, ties, losses));
            }

        }

        return arrayOfArraysOfWTL;
    }

    /**
     * Finds a solution for the different combinations of WinsTiesLosse of all teams. Then sets each team with their
     * respective WinsTiesLosses.
     *
     * @param array, all combinations of WinsTiesLosses of all teams
     * @param auxJ,  auxiliary int that represents the column that is beeing checked
     * @param auxI,  auxiliary int that represents the row that is beeing checked
     */
    public void setTeamsResults(ArrayList<ArrayList<WinsTiesLosses>> array, int auxJ, int auxI) {
        ArrayList<WinsTiesLosses> arrayList = new ArrayList<>();
        int level = 0;
        if (auxJ == array.size()) {
            auxJ = 0;
            level++;
        }
        for (int i = 0; i < array.size(); i++) {
            if (i == auxJ) {
                if (array.get(i).size() > auxI) {
                    arrayList.add(array.get(i).get(auxI));
                    auxI++;
                } else {
                    auxI = 0;
                    auxJ++;
                    i--;
                }
            } else {
                arrayList.add(array.get(i).get(level));
            }
        }
        if (isArrayASolution(arrayList)) {
            for (int i = 0; i < arrayList.size(); i++) {
                teamsPositions[i].setWtl(arrayList.get(i));
            }
        } else {
            setTeamsResults(array, auxJ, auxI);
        }
    }

    /**
     * Method to fin if a combination of WinsTiesLosses is a correct answer
     *
     * @param arrayList combination of WinsTiesLosses
     * @return true if the combination is a correct answer, false otherwise
     */
    private boolean isArrayASolution(ArrayList<WinsTiesLosses> arrayList) {
        int wonMatches = 0;
        int lostMatches = 0;
        int tiedMatches = 0;
        int maxTeamWithTieds = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getTies() > maxTeamWithTieds) {
                maxTeamWithTieds = arrayList.get(i).getTies();
            }
            wonMatches += arrayList.get(i).getWins();
            tiedMatches += arrayList.get(i).getTies();
            lostMatches += arrayList.get(i).getLosses();
        }
        if (wonMatches == lostMatches && wonMatches + lostMatches + tiedMatches == matchesPlayed * 2 && maxTeamWithTieds * 2 <= tiedMatches) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds all the possible results for a Match, based on both teams Wins Ties and Losses
     *
     * @param match, the match that is being analysed
     * @return arrayList of all possible results, 1 if the local wins, 2 if the visit wins or X if it´s a tie
     */
    private ArrayList<Character> possibleResults(Match match) {
        ArrayList<Character> returnValue = new ArrayList<>();
        int localWins = match.getLocalTeam().getWtl().getWins();
        int localTies = match.getLocalTeam().getWtl().getTies();
        int localLosses = match.getLocalTeam().getWtl().getLosses();
        int visitWins = match.getVisitTeam().getWtl().getWins();
        int visitTies = match.getVisitTeam().getWtl().getTies();
        int visitLosses = match.getVisitTeam().getWtl().getLosses();
        if (localWins != 0 && visitLosses != 0) {
            returnValue.add('1');
        }
        if (localTies != 0 && visitTies != 0) {
            returnValue.add('X');
        }
        if (localLosses != 0 && visitWins != 0) {
            returnValue.add('2');
        }

        return returnValue;
    }


    /**
     * Takes the wins ties or losses out of a team, depending on the match result. If a team has no wins, ties or losses
     * is because it´s result for all matches was set.
     *
     * @param c,     Result of the match
     * @param match, match analysed
     */
    public void setAResult(Character c, Match match) {
        if (c == '1') {
            match.getLocalTeam().getWtl().setWins(match.getLocalTeam().getWtl().getWins() - 1);
            match.getVisitTeam().getWtl().setLosses(match.getVisitTeam().getWtl().getLosses() - 1);
        } else if (c == '2') {
            match.getLocalTeam().getWtl().setLosses(match.getLocalTeam().getWtl().getLosses() - 1);
            match.getVisitTeam().getWtl().setWins(match.getVisitTeam().getWtl().getWins() - 1);
        } else if (c == 'X') {
            match.getLocalTeam().getWtl().setTies(match.getLocalTeam().getWtl().getTies() - 1);
            match.getVisitTeam().getWtl().setTies(match.getVisitTeam().getWtl().getTies() - 1);
        }
    }

    /**
     * Changes all teams WinsTiesLosses to their original values
     */
    public void setResultToStart() {
        this.setTeamsResults(this.possibleWTL(), 0, 0);
    }

    public static void run() {
        ScoreBoard scoreBoard;
        ArrayList<String> results = new ArrayList<>();
        while (true) {
            scoreBoard = new ScoreBoard();
            if (scoreBoard.getTeamsInScoreBoard() != -1) {
                scoreBoard.fillTable();
                scoreBoard.fillTheMatches();
                scoreBoard.setTeamsResults(scoreBoard.possibleWTL(), 0, 0);
                scoreBoard.findAnswer(0);
                String result = "Resultado:\t";
                for (int i = 0; i < scoreBoard.getMatchesPlayed(); i++) {
                    result = result + scoreBoard.getMatches()[i].getResult() + " ";
                }
                results.add(result);
            } else {
                if (results.size() > 0) {
                    for (int i = 0; i < results.size(); i++) {
                        System.out.println(results.get(i));
                    }
                } else {
                    System.out.println("There was no valid entry");
                }
                System.out.println("PROCESS FINISHED");
                break;
            }
        }
    }

    /**
     * It sets the result of the matches of the possible solution that is being tested.
     *
     * @return, If it´s a correct solution returns true, otherwise returns false.
     */
    public boolean solve() {
        int aux = 0;
        while (matches.length != aux) {
            ArrayList<Character> arrayList = possibleResults(matches[aux]);
            if (arrayList.size() == 0) {
                return false;
            }
            if (multiResults[aux] == 0) {
                matches[aux].setResult(arrayList.get(0));
                setAResult(arrayList.get(0), matches[aux]);
                aux++;
                continue;
            }
            if (multiResults[aux] == 1) {
                if (arrayList.size() >= 2) {
                    matches[aux].setResult(arrayList.get(1));
                    setAResult(arrayList.get(1), matches[aux]);
                    aux++;
                    continue;
                }
                if (arrayList.size() == 1) {
                    matches[aux].setResult(arrayList.get(0));
                    setAResult(arrayList.get(0), matches[aux]);
                    aux++;
                    continue;
                }
            }
        }
        return true;
    }

    /**
     * Tries all the possible solutions with the solve() method
     *
     * @param repetitions, counts the amount of repetitions to get to the answer, and is used for the setMultiResults()
     *                     method
     */
    public void findAnswer(int repetitions) {
        while (!(solve())) {

            setResultToStart();
            repetitions++;
            setMultiResults(repetitions);

        }
    }

    /**
     * Changes the value of the multiResults array, so all possible solution are checked until the solution is found
     *
     * @param number, the possible solution number
     */
    public void setMultiResults(int number) {
        String binary = Integer.toBinaryString(number);
        int auxBinaryLength = binary.length();
        for (int i = 0; i < binary.length(); i++) {
            multiResults[i] = binary.charAt(auxBinaryLength - 1) - 48;
            auxBinaryLength--;
        }
    }

}
