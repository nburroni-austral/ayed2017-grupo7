package main.tp5;

import main.tp5.models.Station;

import java.util.Scanner;

import static main.tp5.models.Cashier.changePriceTo;

/**
 * Created by Matias on 4/19/17.
 */
public class Main {

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("\n----------------------------------------------------------------");
        System.out.println("Ingrese 1 para realizar una simulación");
        System.out.println("Ingrese 2 para cambiar el precio del pasaje");
        System.out.println("Ingrese 3 para terminar el programa");
        System.out.println("----------------------------------------------------------------\n");
        System.out.print("¿Qué desea hacer?:\t");
        int selection = myScanner.nextInt();

        while(selection != 3) {
            if(selection == 1) {

                System.out.print("\nIngrese cuanto tiempo (en horas) está abierta la estación:\t");
                int openTime = myScanner.nextInt();
                System.out.print("Ingrese cuantos cajeros tiene disponibles:\t");
                int amountOfCashiers = myScanner.nextInt();
                Station station = new Station((openTime * 3600), amountOfCashiers);
                station.run();

                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para realizar otra simulación");
                System.out.println("Ingrese 2 para cambiar el precio del pasaje");
                System.out.println("Ingrese 3 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();

            } else if(selection == 2) {

                System.out.print("\nIngrese el nuevo precio del pasaje:\t");
                int newPrice = myScanner.nextInt();
                changePriceTo(newPrice);

                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para realizar una simulación");
                System.out.println("Ingrese 2 para cambiar nuevamente el precio del pasaje");
                System.out.println("Ingrese 3 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();

            } else {
                System.out.println("Ese no es un número válido!");
                System.out.println("\n----------------------------------------------------------------");
                System.out.println("Ingrese 1 para realizar una simulación");
                System.out.println("Ingrese 2 para cambiar el precio del pasaje");
                System.out.println("Ingrese 3 para terminar el programa");
                System.out.println("----------------------------------------------------------------\n");
                System.out.print("¿Qué desea hacer?:\t");
                selection = myScanner.nextInt();
            }
        }

    }

}
