package main.tp5.models;

import struct.impl.list.DynamicList;

/**
 * Created by Matias on 4/19/17.
 */
public class Station {

    private DynamicList<Cashier> cashiers;
    private int openTime;
    private int actualTime;
    private int amountOfCycles;

    /**
     * Method that constructs a station with a determined amount of cashiers and the amount of time (in seconds) that it will remain open.
     *
     * @param openTime
     * @param amountOfCashiers
     */
    public Station(int openTime, int amountOfCashiers) {
        this.openTime = openTime;
        actualTime = 0;
        amountOfCycles = 0;
        cashiers = new DynamicList<>();
        for (int i = 0; i < amountOfCashiers; i++) {
            cashiers.insertNext(new Cashier());
        }
    }

    /**
     * Method that runs the simulation until the current time matches the open time.
     * Once it's over, the method prints the data of each cashier collected
     */
    public void run() {
        while (actualTime < openTime) {
            doCycle();
        }
        System.out.println("\nSimulation Ended after " + amountOfCycles + " Cycles and with " + cashiers.size() + " Cashiers");
        for (int i = 0; i < cashiers.size(); i++) {
            System.out.println("\nCashier " + (i + 1) + ":\n");
            cashiers.goTo(i);
            cashiers.getActual().printInformation();
        }
    }

    /**
     * Method that enqueues clients, adds idle time (if conditions are met), attends clients and adds time to the actual time counter.
     * When only 30 seconds are left, all clients enqueued are attended immediately.
     */
    private void doCycle() {
        if(openTime - actualTime > 30) {
            enqueueClients();
            addIdleTime();
            attendClients();
            addTime();
            amountOfCycles++;
        } else {
            enqueueClients();
            addIdleTime();
            attendAllClients();
            addTime();
            amountOfCycles++;
        }
    }

    /**
     * Method that adds 10 seconds to the actual time counter.
     */
    private void addTime() {
        actualTime += 10;
    }

    /**
     * Method that enqueues 5 clients to a randomly selected cashier.
     */
    private void enqueueClients() {
        for (int i = 0; i < 5; i++) {
            int index = (int) (Math.random() * cashiers.size());
            cashiers.goTo(index);
            cashiers.getActual().getClientsToAttend().enqueue(new Client(actualTime));
        }
    }

    /**
     * Method that adds idle time to all cashiers, as long as conditions are met.
     */
    private void addIdleTime() {
        for (int i = 0; i < cashiers.size(); i++) {
            cashiers.goTo(i);
            cashiers.getActual().addFreeTime();
        }
    }

    /**
     * Method that iterates through the list of cashiers and makes them attend a client, as long as conditions are met.
     */
    private void attendClients() {
        for (int i = 0; i < cashiers.size(); i++) {
            cashiers.goTo(i);
            cashiers.getActual().attend(actualTime);
        }
    }

    /**
     * Method that iterates through the list of cashiers and makes them attend all clients.
     */
    private void attendAllClients() {
        for (int i = 0; i < cashiers.size(); i++) {
            cashiers.goTo(i);
            cashiers.getActual().attendAll(actualTime);
        }
    }

}
