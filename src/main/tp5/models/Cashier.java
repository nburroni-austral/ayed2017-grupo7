package main.tp5.models;

import struct.impl.queue.DynamicQueue;

/**
 * Created by Matias on 4/19/17.
 */
public class Cashier {

    private static double price = 0.7;
    private int idleTime;
    private DynamicQueue<Client> clientsToAttend;
    private DynamicQueue<Client> clientsAttended;

    /**
     * Method that constructs a cashier with an empty queue of clients to attend and an empty queue of clients attended.
     */
    public Cashier() {
        idleTime = 0;
        clientsToAttend = new DynamicQueue<>();
        clientsAttended = new DynamicQueue<>();
    }

    /**
     * Method that prints the information of the cashier.
     * Currently prints: idle time, clients attended, clients not attended, money made, average waiting time.
     */
    public void printInformation() {
        System.out.print("Total Idle Time (in seconds):\t" + getIdleTime() + "\n");
        System.out.print("Total Amount of Clients Attended:\t" + clientsAttended.size() + "\n");
        System.out.print("Total Amount of Clients Not Attended:\t" + clientsToAttend.size() + "\n");
        System.out.print("Total Amount of Money Made:\t" + amountOfMoneyMade() + "\n");
        System.out.print("Average Client Waiting Time (in seconds):\t" + getAverageWaitingTime() + "\n");
    }

    private int getIdleTime() {
        return idleTime;
    }

    public DynamicQueue<Client> getClientsToAttend() {
        return clientsToAttend;
    }

    public DynamicQueue<Client> getClientsAttended() {
        return clientsAttended;
    }

    /**
     * Method that calculates the average amount of time that the attended clients had to wait in line.
     *
     * @return
     */
    private double getAverageWaitingTime() {
        int totalWaitingTime = 0;
        int amountOfClientsAttended = clientsAttended.size();
        while (clientsAttended.size() > 0) {
            totalWaitingTime += clientsAttended.dequeue().getWaitingTime();
        }
        return totalWaitingTime / amountOfClientsAttended;
    }

    /**
     * Method that calculates the amount of money made by the cashier.
     *
     * @return
     */
    private double amountOfMoneyMade() {
        return clientsAttended.size() * price;
    }

    /**
     * Static method that changes the price of a ticket.
     *
     * @param newPrice
     */
    public static void changePriceTo(double newPrice) {
        price = newPrice;
    }

    /**
     * Method that adds 10 seconds to the cashier's idle time counter if it's clients to attend queue is empty.
     */
    public void addFreeTime() {
        if (clientsToAttend.isEmpty()) {
            idleTime += 10;
        }
    }

    /**
     * Method that has a 30% chance of making a cashier attend a client.
     *
     * @param actualTime Time at which the client was attended by the cashier.
     */
    public void attend(int actualTime) {
        if (clientsToAttend.size() > 0) {
            if (Math.random() < 0.3) {
                Client aux = clientsToAttend.dequeue();
                aux.wasAttended(actualTime);
                clientsAttended.enqueue(aux);
            }
        }
    }

    /**
     * Method that makes a cashier attend all the clients in its queue.
     *
     * @param actualTime Time at which the clients were attended by the cashier.
     */
    public void attendAll(int actualTime) {
        if (clientsToAttend.size() > 0) {
            while (clientsToAttend.size() > 0) {
                Client aux = clientsToAttend.dequeue();
                aux.wasAttended(actualTime);
                clientsAttended.enqueue(aux);
            }
        }
    }

}