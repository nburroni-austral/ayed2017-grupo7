package main.tp5.models;

/**
 * Created by Matias on 4/19/17.
 */
public class Client {

    private int enqueuedTime;
    private int leavingTime;

    public Client(int enqueuedTime) {
        this.enqueuedTime = enqueuedTime;
    }

    public void wasAttended(int time) {
        leavingTime = time;
    }

    /**
     * Method that calculates the amount of time that a client spent waiting while queued, until it was attended.
     *
     * @return Clients waiting time
     */
    public int getWaitingTime() {
        return leavingTime - enqueuedTime;
    }

}