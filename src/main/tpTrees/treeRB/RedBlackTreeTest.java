package main.tpTrees.treeRB;

import java.util.Scanner;

/**
 * Created by Franco Palumbo on 27-Jun-17.
 */
public class RedBlackTreeTest {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        RBTree rbt = new RBTree(Integer.MIN_VALUE);
        System.out.println("Red Black Tree Test\n");

        System.out.println("Enter integer element to insert, 0 to exit");
        int number = scan.nextInt();
        while (number != 0) {
            rbt.insert(number);
            System.out.println("Enter integer element to insert");
            number = scan.nextInt();
        }


        System.out.print("\nPost order : ");
        rbt.postorder();
        System.out.print("\nPre order : ");
        rbt.preorder();
        System.out.print("\nIn order : ");
        rbt.inorder();

    }

}
