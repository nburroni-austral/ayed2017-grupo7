package main.tpTrees.tree234;

/**
 * Created by Matias on 5/30/17.
 */
public abstract class Node {

    private Node father;
    public int type;
    int x;
    int y;

    public abstract Node search(Comparable c);

    public abstract boolean isLeaf();

    public abstract Node insert(Object object);

    public abstract void setChild(Comparable o, Node child);

    public abstract void print();

    public abstract Object[] getData();

    public Node getFather() {
        return father;
    }

    public void setFather(Node father) {
        this.father = father;
    }


}
